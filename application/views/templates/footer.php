
<div class="footer">


    <div class="friendlywifi container-fluid">
        <div class="row">
            <div class="center-block" style="max-width: 500px;">
              <?php
              if ($this->config->item('settings_parsed')->friendly_wifi) :
              ?>
                <div class="col-xs-6">
                  <a href="http://www.friendlywifi.com/"><img class="center-block friendly" src="/assets/images/friendly_wifi.png" alt="Friendly WiFi" /></a>
                </div>
              <?php
              endif;
              ?>
              <div class="col-xs-5">
                <a href="https://www.wifispark.com"><img class="img-responsive" src="/assets/images/wifispark.png" alt="WiFiSPARK logo" /></a>
              </div>
            </div>
        </div>
    </div>
    <?php
    if ($this->config->item('settings_parsed')->friendly_wifi) :
    ?>

    <?php
    endif;
    ?>



  <div class="wifi_support">
    <div class="inner text-right">
        <p>
            WiFi Support <i class="fa fa-angle-down" aria-hidden="true"></i><i class="fa fa-angle-down" aria-hidden="true"></i>
        </p>
    </div>
    <div class="body">

    </div>
  </div>
</div>
<div id="termsModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:red; font-size:28px; font-weight:bold">×</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body row"></div>
        </div>
    </div>
</div>
<div class="spinner-container" style="display: none;">
  <img class="spinner" src="/assets/images/spinner.gif" />
</div><script type="text/javascript">
$(document).ready(function () {
$('.menu-burger').click(function () {
    $('.menu').addClass('menu-open');
});
$('.menu .close').click(function () {
    $('.menu').removeClass('menu-open');
});
$.ajax({
    url: "/pageview/<?php echo $page; ?>"
});
});
$('.terms-modal').on('click', function(e){
e.preventDefault();
$href = $(this).attr('href');
$('.modal').fadeIn('slow');
$('.modal .modal-body').load($href + ' .content .justified');
$('.modal .modal-title').load($href + ' .heading div');
});
$('.modal .close').on('click', function(){
$('.modal').fadeOut('slow');
});
if($('body').height() > (window.innerHeight - 38)) {
$('body').css({
    'min-height': '100vh'
});
}
</script>

<script src="/assets/lib/bootstrap/js/bootstrap.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/lib/js/ie10-viewport-bug-workaround.js"></script>
<script src="/assets/lib/js/vegas.js"></script>
<script src="/assets/lib/bootstrap/js/bootstrapValidator.js" type="text/javascript"></script>
<script>
$(document).ready(function() {




        // Apply BootstrapValidator plugin to each Portal form
        $('form.portal-form').bootstrapValidator({
            message: 'This value is not valid',
            trigger: 'blur change',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh',
            },
            // Field validators are defined via HTML5 attributes along with
            // the error messages for I18N apart from gender as we need to
            // check something has been selected.
            fields: {
                gender: {
                    validators: {
                        callback: {
                            callback: function(value, validator) {
                                // Get the selected options
                                var options = validator.getFieldElements('gender').val();
                                return (options != 0);
                            }
                        }
                    }
                },
            }
        }).on('error.validator.bv', function(e, data) {
            // Triggered on field validation failure
            // $(e.target)    --> The form instance
            // data.field     --> The field name
            // data.element   --> The field element
            // data.validator --> The validator name

            // If there is an invalid field, hide the AJAX response error
            // ready to show the validators error. Only 1 error at a time.
            $('#form-error-message').val('')
                                    .addClass('hidden');

            // Only show one validator error message at a time.
            // A field may have multiple validators, don't swamp the user.
            data.element
                .data('bv.messages')
                // Hide all the messages
                .find('.help-block[data-bv-for="' + data.field + '"]').hide()
                // Show only message associated with current validator
                .filter('[data-bv-validator="' + data.validator + '"]').show();

        }).on('success.validator.bv', function(e, data) {
            // Triggered on field validation success
        }).on('success.form.bv', function(e) {
            // Triggered on form validation success

            /*
             * Handle clicking Submit when the fields are considered
             * valid. Once submitted we either proceed to the Portal_Process_*
             * view or display an error.
             *
             * WARNING: Do not use the properties of a form, such as submit, reset, length, method to set
             * the name or id attribute of form, field elements. Name conflicts can cause the problem.
             * http://bootstrapvalidator.com/getting-started/#name-conflict
             *
             */

            // Get the form instance
            var $form = $(e.target);
            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            if (!bv.getSubmitButton()) {
                // Submit not clicked - avoid issue when calling bv.isValid() automatically submitting form.

                return false;
            }


            // If the form supports AJAX, otherwise normal POST
            if ($form.data().hasOwnProperty("ajax")) {

                // Prevent form submission for AJAX requests
                e.preventDefault();

                // Use Ajax to submit form data
                $.post(SPARK_Data.url + '&ajax=form_submit', $form.serialize(), function(result) {

                    if ( result.valid ) {
                        // Redirect to process view to finish logging in
                        window.location.href = result.redirect_next_step;
                    } else {

                        // We may have individual field errors, or a form error
                        // that relates to the whole form
                        if ( "field_errors" in result && result.field_errors ) {
                            for (var prop in result.field_errors) {
                                if (result.field_errors.hasOwnProperty(prop)) {
                                    // Each error div is named with the suffix -$field_name
                                    var errors = $('#form-error-message' + '-' + prop);
                                    $(errors).closest('.form-group').removeClass('has-success')
                                                                    .addClass('has-error')
                                                                    .find('i.form-control-feedback')
                                                                    .removeClass('glyphicon-ok')
                                                                    .addClass('glyphicon-remove');
                                    $(errors).html(result.field_errors[prop]).removeClass('hidden');
                                }
                            }
                        } else {
                            // Display form errors within a form-group
                            var errors = $('#form-error-message');
                            $(errors).closest('.form-group').removeClass('has-success')
                                                            .addClass('has-error')
                                                            .find('i.form-control-feedback')
                                                            .removeClass('glyphicon-ok')
                                                            .addClass('glyphicon-remove');
                            $(errors).html(result.message).removeClass('hidden');
                        }

                        // For subscribers, display a Buy Time option if they have run out
                        if ( "no_time_left" in result && result.no_time_left ) {
                            $('#buy_more_time').removeClass('hidden');
                        }
                    }

                }, 'json');
            }
        });

        // Clear the current input error field if the red X is clicked on the feedback icons.
        $('div.form-group').on('click', 'i.glyphicon-remove', function() {
            var field = '#' + $(this).data('bv-icon-for');
            if ($(field).is('input')) {
                $(field).val('').focus();
            }
        });

});

$(document).ready(function(){
    $('.wifi_support .body').load('/support .content > *');
});

$('.wifi_support p').on('click', function(){
    $('.wifi_support .body').slideToggle('slow', function() {
        $('.wifi_support .body').toggleClass('populated');
    });
});

$(document).on('click', '.terms-modal' , function(e){
    e.preventDefault();
    e.stopPropagation();
    $href = $(this).attr('href');
    $('.modal').fadeIn('slow');
    $('.modal .modal-body').load($href + ' .content .justified');
    $('.modal .modal-title').load($href + ' .heading div');
});

</script>
<script>
  $("form").submit(function(){
    $('.spinner-container').show();
  });
</script>
<script>
    $(document).on('click', '.terms-modal', function(event) {
        event.preventDefault();
        $('.big-modal').fadeIn('slow');
        $('.big-modal .modal-wrapper').load('/assets/terms-and-conditions-English-2015.html');
    });
</script><script>
    $(document).on('click', '.help-modal', function(event) {
        event.preventDefault();
        $('.big-modal').fadeIn('slow');
        $('.big-modal .modal-wrapper').load('/assets/help.html');
    });
</script><script>
    $(document).on('click', '.close-button', function(event) {
        event.preventDefault();
        $('.big-modal').fadeOut('slow');
    });
</script>
<script>
    $('.languagepicker li:first-child').on('click', function(){
        $('.languagepicker li:not(.selected)').toggle();
        $('.languagepicker').addClass('open');
        var current = 1;
        $('.languagepicker li:not(.selected)').each( function(){
            $(this).css({
                'top': current * 32
            });
            current++;
        });
    });
</script>
<script type="text/javascript" src="/assets/lib/bootstrap/js/portal-bootstrap.js"></script>
</body>
</html>
