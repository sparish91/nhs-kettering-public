<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>WiFi registration validation required.</title>
    </head>
    <body>
        <p style="background-color: #2d3e50; width: 100%; padding: 20px;">
            <a href="http://www.wifispark.com/"><img src="cid:<?php echo $logo_cid; ?>" alt="WiFiSPARK logo" /></a>
        </p>
        <h3>Dear <?php echo $first_name; ?></h3>
        <p>
            Click the link below to validate the device you registered on the network.
        </p>
        <p>
            <a href="<?php echo $link ?>">Get me online!</a>
        </p>
        <p>
            Thank you for connecting.
        </p>
        <p>
            <strong>WiFiSPARK Team.</strong>
        </p>
        <hr />
        <?php
            if ($friendly_wifi) :
        ?>
        <p>
            <a href="http://www.friendlywifi.com/"><img src="cid:<?php echo $friendly_cid; ?>" alt="Friendly Wifi" /></a>
        </p>
        <?php
            endif;
        ?>
    </body>
</html>
