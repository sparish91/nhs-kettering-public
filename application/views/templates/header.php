<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" href="/favicon.ico" />
<title>NHS WiFi | Register</title>
<link href="/assets/lib/bootstrap/css/bootstrap.css" rel="stylesheet" />
<link href="/assets/lib/bootstrap/css/bootstrap-social.css" rel="stylesheet" />
<link href="/assets/css/font-awesome.css" rel="stylesheet">
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="/assets/lib/css/ie10-viewport-bug-workaround.css" rel="stylesheet" />
<link rel="stylesheet" href="/assets/css/vegas.css">
<link rel="stylesheet" href="/assets/css/main.css">
<link rel="stylesheet" href="/assets/css/animate.css">
<link rel="stylesheet" href="/assets/css/flags.min.css">
<script src="/assets/lib/js/jquery.min.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="/assets/lib/js/html5shiv.min.js"></script>
  <script src="/assets/lib/js/respond.min.js"></script>
<![endif]-->
<link rel="apple-touch-icon" sizes="57x57" href="/assets/icons/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="60x60" href="/assets/icons/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon" sizes="72x72" href="/assets/icons/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="76x76" href="/assets/icons/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon" sizes="114x114" href="/assets/icons/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon" sizes="120x120" href="/assets/icons/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon" sizes="144x144" href="/assets/icons/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon" sizes="152x152" href="/assets/icons/apple-touch-icon-152x152.png" />
<link rel="apple-touch-icon" sizes="180x180" href="/assets/icons/apple-touch-icon-180x180.png" />
<link rel="apple-touch-icon-precomposed" href="/assets/icons/apple-touch-icon-precomposed.png" />
<link rel="icon" type="image/png" href="/assets/icons/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="/assets/icons/favicon-194x194.png" sizes="194x194" />
<link rel="icon" type="image/png" href="/assets/icons/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="/assets/icons/android-chrome-192x192.png" sizes="192x192" />
<link rel="icon" type="image/png" href="/assets/icons/favicon-16x16.png" sizes="16x16" />
<link rel="manifest" href="/assets/icons/manifest.json" />
<link rel="mask-icon" href="/assets/icons/safari-pinned-tab.svg" color="#ff0000" />
<link rel="shortcut icon" href="/assets/icons/favicon.ico" />
<meta name="msapplication-TileColor" content="#ffffff" />
<meta name="msapplication-TileImage" content="/assets/icons/mstile-144x144.png" />
<meta name="msapplication-config" content="/assets/icons/browserconfig.xml" />
<meta name="theme-color" content="#ffffff" />
</head>
<body>

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
     <![endif]-->

     <header class="container-fluid">
        <div class="row">
          <div class="container">
            <div class="pull-left">
              <h3>Register for free<br />
              NHS Wi-Fi</h3>
            </div>
            <img src="/assets/images/nhslogo.png" alt="">

          </div>
        </div>
    </header>
    <div class="banner">
      <img class="img-responsive center-block" src="/assets/images/banner.jpg" />
    </div>
