<section id="spark_form" class="container-fluid">
    <div class="row">
        <div id="spark_inner">
            <div id="spark-box-start" class="panel panel-default">
                <div class="panel-heading">
                    <ul class="breadcrumb">
                        <li class=" active">
                            <a href="#">
                                <h1>Free <span>NHS </span>WiFi</h1>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <p style="font-size: 16px;"><b>Welcome to our public WiFi Service</b></p>

                    <p>To connect, please ensure you agree to the<br /> <a href="/terms_of_use" class="terms-modal">terms of use</a>, then click the button below.</p>
                    <!-- <p>Or login with <a href="<?php echo htmlspecialchars(""); ?>">Facebook</a>!</p> -->

                    <p></p>
<?php include("include/formerrors.php"); /////////////////////////// form error messages ?>
<?php
    // show flashdata messages if necessary
    if($this->session->flashdata('msg')):
?>
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
</div>
<?php
    endif;
?>
<?php include("include/formgenerator.php"); /////////////////////////// form itself ?>
                    <br />
                </div>
            </div>
        </div>
    </div>
</section>
