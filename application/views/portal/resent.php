<div class="content center-block">
<div class="row heading">
    <div class="col-xs-12">
        WiFi Registration
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <p>Validation email has been resent.</p>
        <p>
            Please check your email and click on the validation link to be granted <?php echo $validated_days; ?> days of free internet access.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        <a class="btn register" href="/support">WiFi support</a>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
