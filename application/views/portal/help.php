<div class="content center-block">
<div class="row heading">
    <div class="col-xs-12">
        Help &amp; FAQs
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
<dl>
    <dt>Can I use more than one device?</dt>
    <dd>Yes, although you will need to go through the registration on each device separately.</dd>
    <dt>How can I get help?</dt>
    <dd>Please read the <a href="/support">support page</a>, if you require any further assistance with the WiFi, contact WiFi SPARK on <a href="mailto:support@wifispark.com">support@wifispark.com</a> or call them on <a href="tel:03448489555">0344 848 9555</a>.</dd>
    <dt>What is WiFi?</dt>
    <dd>WiFi is short for Wireless Fidelity, a means by which devices can access Internet or other network based services without the need for a network or modem cable.</dd>
    <dt>Can I get my Webmail like Gmail?</dt>
    <dd>Yes! We fully support web mail access to Outlook, Yahoo Mail, and Gmail.</dd>
    <dt>Can I use a VPN?</dt>
    <dd>YES! In fact if you are connecting to the Internet through your company laptop we would recommend using a VPN client for secure access to company intranets and mail servers. Check that your company VPN supports NAT traversal and allows multiple connections from a single IP address. Check with your IT staff. Please ensure that you are logged in through the portal page before you use your VPN.</dd>
    <dt>The access speed is really slow.</dt>
    <dd>If the access speed is slow, you are probably in a poor signal strength area. Try moving closer to the access point. Check your wireless icon. It will have some method of detecting signal strength which will allow you to move around until you are in a good signal strength area.</dd>
    <dt>Are there any data usage limits?</dt>
    <dd>As detailed in our <a href="/terms_of_use">Terms and Conditions</a> there is a fair usage policy of 20GB per month from the WiFi service.</dd>
    <dt>Why are some sites blocked and I cannot gain access to them?</dt>
    <dd>WiFi SPARK block certain categories to help protect users from accessing malicious or inappropriate content.  If you feel that a site is incorrectly blocked, please let us know so that we can investigate. Please note that we are not responsible for the security of your data or your device. The security of your connected device is your responsibility, and we recommend that your firewall and antivirus software is correctly configured and up to date.</dd>
    <dt>What speeds can I expect?</dt>
    <dd>We cannot guarantee the speed that you receive but we aim to offer our customers speeds of around <?php echo $download_speed; ?>. There are many variables which can affect the internet data speeds. These include the number of people browsing on our network at that given time and also the quality of the wireless signal.</dd>
</dl>
<p><span class="glyphicon glyphicon-chevron-left"></span><a href="/">Return to registration page</a></p>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('dd').hide();
        $('dt').on('click', function () {
            $('dd').hide();
            $(this).next().fadeIn(500);
        });
    });
</script>
