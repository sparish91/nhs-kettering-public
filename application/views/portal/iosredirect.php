<html>
<head>
<title>Landing page</title>
<style>
#link, #done {
    display: none;
}
#done {
    max-width: 100vw;
    margin: auto;
}
</style>
</head>
<body>
  <div class="row">
    <div class="registration">
      <div class="row heading">
        <div class="col-xs-12 text-center">
          <h1 class="header">You are connected!</h1>
          <h5 id="done">Please close this window to continue</h5>
        </div>
        <!--<div class="footer">
            <p>Wifi Helpdesk 0344 848 9555</p>
            <img src="../../assets/images/wifilogo.png">
        </div>-->
      </div>
    </div>
  </div>
<a href="<?php echo $_SESSION['url']; ?>" id="link">Link</a>
<script src="/assets/lib/js/jquery.min.js"></script>
<script type="text/javascript">
<!--
$('document').ready(function () {
    setTimeout(function () {
        document.getElementById("link").click();
    }, 2000);
    setTimeout(function () {
      $("#done").show();
  }, 2500);
});
-->
</script>
</body>
</html>
