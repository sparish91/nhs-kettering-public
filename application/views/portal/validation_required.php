<div class="content center-block">
<div class="row heading">
    <div class="col-xs-12 text-center">
        <h3>Email not validated</h3>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <p>
            This device has previously been registered and you should have received an email with a validation link.  Please check your email (including the Spam folder).
        </p>
    </div>
    <div class="col-xs-12 top-gap">
        <a href="/resend" id="resend">Resend activation email</a><br />
        <a href="/reset">Register device again</a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        <a class="btn register" href="/support">WiFi support</a>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
<script src="/assets/lib/js/jquery.FlowupLabels.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#resend').attr('href', '#').on('click', function() {
            if ($('#verify').length == 0) {
                $('#resend').after('<div id="verify"><form action="/verify" method="post" class="FlowupLabels"><div class="form-group fl_wrap"><label for="email" class="fl_label">Your email address :</label><input type="text" name="email" class="fl_input" required pattern="^[a-zA-Z0-9\._%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$" /></div><button class="btn btn-success" type="submit">Verify Email</button></form></div>');
                $('#verify').hide().fadeIn(500);
                $('.FlowupLabels').FlowupLabels();
                $('form').on('submit', showSpinner);
            }
        });
        $('body').removeClass('no-js');
    });
    function showSpinner() {
        $('body').append('<div class="spinner-container"><div class="spinner"><img src="/assets/images/spinner.gif" /></div></div>');
        $('body').addClass('noscroll');
    }
</script>
