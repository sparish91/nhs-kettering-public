<div class="content center-block">
<div class="row heading">
    <div class="col-xs-12 text-center">
        <h3>Welcome back</h3>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 top-gap">
        <p>Please click the button below to continue.</p>
        <p class="text-right" style="width: 87.5%;margin: 16px auto 0;"><a style="color: #005EB8;" href="/reset">Forget this device</a></p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        <a class="btn register" href="/proceed">Proceed to internet</a>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
