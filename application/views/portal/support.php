<div class="content center-block">
<div class="row">
    <div class="col-xs-12">
        <p>Welcome to the WiFi service brought to you by WiFi SPARK. The service is free and very easy to use. Simply follow the instructions below to connect:</p>
        <ol>
            <li>
                Ensure you have read and agreed to the <a class="terms-modal" href="/terms_of_use">terms of use</a>.
            </li>
            <li>
                Click the Connect button
            </li>
        </ol>
        <p>Your device will be remembered for 365 days. After this time, you will simply need to re-register again.</p>
        <p>If you require any assistance with the WiFi service, please call the WiFi SPARK Helpdesk on 0344 848 9555.</p>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
