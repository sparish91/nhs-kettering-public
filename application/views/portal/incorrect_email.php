<div class="content center-block">
<div class="row heading">
    <div class="col-xs-12">
        Incorrect email supplied
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <p>
            The email you submitted does not match the email you used to register.<br />
            <a href="/reset">Please register your device again</a>
        </p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        <a class="btn register" href="/support">WiFi support</a>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
