<div class="content center-block">
<div class="row heading">
    <div class="col-xs-12 text-center">
        <h3>Thank you</h3>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 top-gap">
        <p>Your details are now validated and you will now benefit from <?php echo $validated_days; ?> days of internet access.</p>
        <p class="text-right" style="width: 87.5%;margin: 16px auto 0;"><a style="color: #005EB8;" href="/reset">Forget this device</a></p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        <a class="btn register" href="/connect/<?php echo $mac; ?>">Proceed to internet</a>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('body').append('<img src="http://1.1.1.1/" style="display: none;" />');
});
</script>
