<?php
    $form_errors = validation_errors();
    // show any server form validation errors
    if ($form_errors):
?>
<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-danger" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $form_errors; ?>
        </div>
    </div>
</div>
<?php
    endif;
?>
