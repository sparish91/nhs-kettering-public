<?php
    echo form_open('portal/register', array('class' => 'FlowupLabels', 'role' => 'form'));
?>
<div>
    <div class="col-xs-12">
<?php
    // get the form details from parsed json settings
    $form_items = $this->config->item('settings_parsed')->form;
    $showRequiredMsg = false;
    foreach ($form_items as $form_item) {
?>
            <div class="form-group has-feedback<?php
            if (form_error($form_item->name)) echo ' has-error';
            echo ('select' == $form_item->type)?' sel_wrap':' fl_wrap';
            ?>" id="<?php echo $form_item->name; ?>-wrapper">
                <label class="<?php echo ('select' == $form_item->type)?'label-select':'fl_label'; ?>" for="<?php echo $form_item->name; ?>"><?php

                if ($form_item->required) {
                    echo '* ';
                }
                    echo $form_item->label . ' :';
                ?></label>
                <?php
                    if ('select' != $form_item->type) :
                ?>
                <input class="form-control fl_input"
                 type="<?php echo $form_item->type ?>"
                 id="<?php echo $form_item->name; ?>"
                 name="<?php echo $form_item->name; ?>"
                 value="<?php echo set_value($form_item->name); ?>"
                 placeholder="<?php echo $form_item->placeholder; ?>"
                 onchange="<?php echo $form_item->on_change; ?>"
                 data-error="<?php echo $form_item->client_validation_error; ?>"
                 <?php
                    if ($form_item->client_validation_regex) {
                        echo 'pattern="' . $form_item->client_validation_regex . '"';
                    }
                    if ($form_item->required) {
                        echo ' required';
                        $showRequiredMsg = true;
                    }
                 ?> />
                <?php
                    else :
                        echo '<select id="' . $form_item->name . '"';
                        echo ' name="' . $form_item->name . '"';
                        echo ' onchange="' . $form_item->on_change . '"';
                        if ($form_item->required) {
                            echo ' required';
                            $showRequiredMsg = true;
                            echo ' data-error="' . $form_item->client_validation_error . '"';
                        }
                        echo ' class="form-control">';
                        foreach ($form_item->options as $option) {
                            echo '<option value="' . $option->value . '"' . (($option->value == $form_item->default)?'selected':'') . (isset($option->disabled)?'disabled':'') . '>' . $option->label . '</option>';
                        }
                        echo '</select>';
                ?>
                <?php
                    endif;
                ?>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
<?php
    }   // foreach
?>
            <div class="top-gap">
<?php
    // get marketing options from parsed json settings
    $marketing_options = $this->config->item('settings_parsed')->marketing_options;
    foreach ($marketing_options as $option) {
        if (isset($_POST['mac'])) {
            // we've come back around to the form
            // if the $_POST item is set, then the marketing option is ticked, otherwise it was unticked
            $checked = isset($_POST[$option->name]);
        } else {
            // first time we've seen the form
            // set to default value
            $checked = ('on' == $option->default);
        }
         $isRequired = ($option->required)?'required data-error="' . $option->client_validation_error . '"':'';
?>
         <div class="form-group has-feedback">
            <input type="checkbox" <?php echo $isRequired; ?> id="<?php echo $option->name; ?>" name="<?php echo $option->name; ?>" onclick="<?php echo $option->on_change; ?>"<?php if ($checked) echo ' checked';?> />
            <label for="<?php echo $option->name; ?>" class="marketing"><span></span><?php echo $option->label; ?></label>
            <div class="help-block with-errors"></div>
        </div>
<?php
    }   // foreach
    if ($this->config->item('settings_parsed')->agree_terms) :
        $showRequiredMsg = true;
        if (isset($_POST['mac'])) {
            // we've come back around to the form
            // if the $_POST item is set, then the marketing option is ticked, otherwise it was unticked
            $checked = isset($_POST['agree_terms']);
        } else {
            $checked = false;
        }
?>
            <div class="form-group relative">
                <input type="checkbox" id="agree_terms" name="agree_terms" <?php if ($checked) echo ' checked';?> required data-error="You must agree the terms &amp; conditions" />
                <label for="agree_terms" class="marketing"><span></span>By submitting your details you are agreeing to our Terms &amp; Conditions *</label>
                <div class="help-block with-errors"></div>
            </div>
<?php
    endif;
?>
            </div>
<?php
    // add "* indicates required" notice
    if ($showRequiredMsg):
?>
            <div class="marketing">
                * indicates mandatory information
            </div>
<?php
    endif;
?>
            <div class="text-center">
                <button class="btn register" id="proceed" type="submit">Accept & connect</button>
            </div>
</div>
</div>
<input type="hidden" name="mac" value="<?php echo $_SESSION['mac']; ?>" />
</form>
