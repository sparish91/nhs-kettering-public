<div class="content center-block">
<div class="row heading">
    <div class="col-xs-12 text-center">
        <h2 style="margin-top: 0;">WiFi Registration</h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <p>Thank you for registering.</p>
        <p>
            Please click the button below to continue.
        </p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 text-center">
        <a class="btn register" href="<?php echo $landing_page; ?>">Proceed to internet</a>
    </div>
</div>
</div>
<script src="/assets/lib/js/jquery.min.js"></script>
