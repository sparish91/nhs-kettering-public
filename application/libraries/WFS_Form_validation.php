<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Provides server side form validation rules that align with the SPARK API validation rules.
 *
 * @package    Spark
 * @copyright  2003-2016 WifiSpark Limited. All rights reserved.
 * @license    http://www.wifispark.com/license/spark-1.00.txt  SPARK License 1.00
 * @version    GIT: $Id$
 */

class WFS_Form_validation extends CI_Form_validation
{
    public function __construct($rules = array()) {
        parent::__construct($rules);
    }

    /**
     * valid_name($name)
     *
     * @param string $name - the name to be validated
     * @return boolean - true if the name is valid
     */
    public function valid_name($name) {
        if (preg_match("/^[a-zA-Z\s\'-]{2,100}$/", $name)) return true;

        $this->set_message('valid_name', 'Your %s is too short (minimum 2 characters) or contains illegal characters');
        return false;
    }

    /**
     * valid_date($date)
     *
     * @param string $date - the date to be validated
     * @return boolean - true if the date is valid
     */
    public function valid_date($date) {
        $date_parts = explode('/', $date);
        if (3 == count($date_parts)) {
            if (checkdate($date_parts[1], $date_parts[0], $date_parts[2])) {
                return true;
            }
        }
        $this->set_message('valid_date', 'Date format must be Day/Month/Year eg. 16/07/1969');
        return false;
    }

    /**
     * valid_mobile($number)
     *
     * @param string $number - the number to be validated
     * @return boolean - true if mobile number is valid
     */
    public function valid_mobile($number) {
        if (preg_match('/^[\+]{0,1}[0-9]{6,12}$/', $number)) return true;

        $this->set_message('valid_mobile', 'You must enter a valid %s (eg. 03458489555)');
        return false;
    }

    /**
     * valid_postcode($postcode)
     *
     * @param string $postcode - the postcode to be validated
     * @param boolean - true if the postcode is valid
     */
    public function valid_postcode($postcode) {
        if (preg_match('/^(GIR 0AA)|(((A[BL]|B[ABDHLNRSTX]?|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]?|F[KY]|G[LUY]?|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]?|M[EKL]?|N[EGNPRW]?|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKLMNOPRSTY]?|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)[1-9]?[0-9]|((E|N|NW|SE|SW|W)1|EC[1-4]|WC[12])[A-HJKMNPR-Y]|(SW|W)([2-9]|[1-9][0-9])|EC[1-9][0-9]) [0-9][ABD-HJLNP-UW-Z]{2})$/', $postcode)) return true;

        $this->set_message('valid_postcode', 'You must enter a valid %s (eg. EX2 8PW)');
        return false;
    }
}
