<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Subscriber model
 *
 * @package    Spark
 * @copyright  2003-2016 WifiSpark Limited. All rights reserved.
 * @license    http://www.wifispark.com/license/spark-1.00.txt  SPARK License 1.00
 * @version    GIT: $Id$
 */

class Subscriber extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $params = json_decode(json_encode($this->config->item('api_settings')), true);
        $this->load->library('sparkapi', $params);
    }

    /**
     * create($subscriber)
     * Create a user in SPARK and give them 15 minutes of Internet
     *
     * @param Array $subscriber - POST data from registration form
     *
     * @return integer/false - subscriber ID or false on failure
     */
    public function create($subscriber)
    {
        $rip = (isset($_SESSION['rip']))?$_SESSION['rip']:$this->config->item('settings_parsed')->default_hotspot_ip;
        $result = false;
        $create_data = Array(
            'login' => $this->config->item('settings_parsed')->login_prefix . '-' . $subscriber['mac'],
            'password' => $subscriber['mac'],
            'hotspot_ip' => $rip,
            'agree_terms_and_conditions' => 1,
            'user_def01' => $subscriber['mac'],                     // mac address
            'user_def02' => date('Y-m-d-H-i-s'),                    // last registration date
            'user_def03' => 1                                       // validated flag
        );
        // send interface_id if needed
        $interface_id = $this->sparkapi->getInterfaceID($rip);
        if ($interface_id != '') {
            $create_data['interface_id'] = $interface_id;
        }
        // insert form data into user create data
        $form = $this->config->item('settings_parsed')->form;
        foreach ($form as $form_item) {
            $create_data[$form_item->name] = $subscriber[$form_item->name];
        }
        // insert marketing options into user create data
        $marketing_options = $this->config->item('settings_parsed')->marketing_options;
        foreach ($marketing_options as $option) {
            $create_data[$option->name] = isset($subscriber[$option->name])?'1':'0';
        }
        // create our user in SPARK
        $subscriber_id = $this->sparkapi->createSubscriber($create_data);

        // drop user_def fields for custom fields for analytics
        foreach($create_data as $user_data => $user_value) {
            switch($user_data) {
                case 'user_def01' :
                    $create_data['user_device_mac_address'] = $create_data['user_def01'];
                    unset($create_data['user_def01']);
                    break;
                case 'user_def02' :
                    $create_data['user_registration_date'] = $create_data['user_def02'];
                    unset($create_data['user_def02']);
                    break;
            }
        }

        //More info about the fields can be found at:
        //http://new-wiki/doku.php?id=development:splunk&s[]=analytics
        $analyticsData = array();
        $analyticsData['form_data'] = $create_data;
        $analyticsData['type'] = 'Portal';
        $analyticsData['view'] = 'NewUser';
        $analyticsData['device_mac_address'] = $subscriber['mac'];
        $analyticsData['view_valid'] = true;
        $analyticsData['doing_ajax'] = false;
        $analyticsData['username'] = $create_data['login'];
        $analyticsData['user_id'] = $subscriber_id;

        $this->analytics($analyticsData);

        if ($subscriber_id) {
            // user created successfully so give them some time to compete validation
            if ($this->activate($subscriber_id, $subscriber['mac'], $this->config->item('settings_parsed')->validation_access_time)) $result = $subscriber_id;
        }
        return $result;
    }

     /**
     * analytics($payload)
     * Send analytics data to Splunk via SparkAPI
     *
     * @param array $payload - analytics data payload
     */
    public function analytics($payload)
    {
        try {
            $rip = (isset($_SESSION['rip']))?$_SESSION['rip']:$this->config->item('settings_parsed')->default_hotspot_ip;
            $this->sparkapi->sendAnalytics($payload, $rip, $this->sparkapi->getInterfaceID($rip));
        } catch(Exception $e){
            log_message('debug', $e->getMessage());
        }
    }

    /**
     * deleteMAC($subscriber_id)
     * "delete" a MAC from SPARK
     *
     * @param integer $subscriber_id - unique subscriber ID
     */
    public function deleteMAC($subscriber_id)
    {
        $update = Array(
            'login' => $this->config->item('settings_parsed')->login_prefix . '-deleted-' . md5(microtime())
        );
        $this->sparkapi->updateSubscriber($subscriber_id, $update);
        unset($_SESSION['subscriber_token']);
        unset($_SESSION['subscriber_token_expires']);
        log_message('debug', 'deleteMAC : subscriber ID = ' . $subscriber_id);
    }

    /**
     * validate($subscriber_id, $mac)
     * Update a subscriber record to record them as email validated
     *
     * @param integer $subscriber_id - subscriber unique id
     *
     * @return stdClass Object $subscriber - returned subscriber details from SPARK
     */
    public function validate($subscriber_id)
    {
        $update = Array(
            'user_def03' => '1'                 // validated flag
        );
        $this->sparkapi->updateSubscriber($subscriber_id, $update);

        return $this->getByID($subscriber_id);
    }

    /**
     * reactivate($hotspot_ip, $mac)
     * Start subscriber session again
     *
     * @param string $hotspot_ip - IP address for hotspot
     * @param string $mac - MAC address of device
     *
     * @return boolean successfully reactivated subscriber
     */
    public function reactivate($hotspot_ip, $mac)
    {
        $rip = (isset($_SESSION['rip']))?$_SESSION['rip']:$this->config->item('settings_parsed')->default_hotspot_ip;
        return $this->sparkapi->startSession($hotspot_ip, $mac, $this->config->item('settings_parsed')->login_prefix . '-' .$mac, $mac, $this->sparkapi->getInterfaceID($rip));
    }

    /**
     * provision($subscriber_id, $mac)
     * Give subscriber 365 days on the Internet
     *
     * @param integer $subscriber_id - subscriber unique id
     * @param string $mac - MAC address of device
     *
     * @return see activate()
     */
    public function provision($subscriber_id, $mac)
    {
        return $this->activate($subscriber_id, $mac, $this->config->item('settings_parsed')->validated_access_time);
    }

    /**
     * activate($subscriber_id, $mac, $seconds)
     * Activate a subscriber on to the Internet
     *
     * @param integer $subscriber_id - unique subscriber id
     * @param string $mac - MAC address of device
     * @param integer $seconds - number of seconds of Internet access given to subscriber
     *
     * @return boolean true for successful activation
     */
    public function activate($subscriber_id, $mac, $seconds)
    {
        $bandwidth = $this->config->item('settings_parsed')->bandwidth;
        $activation = array(
            'activation' => array(
                'range' => array($this->config->item('settings_parsed')->activation_range),
                'duration' => $seconds,
                'max_users' => 1,
                'bandwidth' => array(
                    'up' => $bandwidth->up,
                    'down' => $bandwidth->down
                )
            )
        );

        $success = false;
        if ($this->sparkapi->activateSubscriber($subscriber_id, $activation)) {
            log_message('debug', 'Subscriber activated OK. Trying authorisation...');
            $rip = (isset($_SESSION['rip']))?$_SESSION['rip']:$this->config->item('settings_parsed')->default_hotspot_ip;
            if ($this->reactivate($rip, $_SESSION['mac'])) {
                log_message('debug', 'Subscriber authorised OK.');
                $success = true;
            }
        }
        return $success;
    }

    /**
     * getByID($subscriber_id)
     * Get subscriber record from SPARK based on unique id
     *
     * @param integer $subscriber_id - unique subscriber id
     *
     * @return stdClass Object $subscriber - returned subscriber details from SPARK
     */
    public function getByID($subscriber_id)
    {
        return $this->sparkapi->getSubscriber($subscriber_id);
    }

    /**
     * getByMAC($mac)
     * Get subscriber record from SPARK based on MAC address
     *
     * @param string $mac - MAC address of device
     *
     * @return Array $subscriber - returned subscriber details from SPARK
     */
    public function getByMAC($mac)
    {
        $search = Array('login' => $this->config->item('settings_parsed')->login_prefix . '-' . $mac);
        $subscriber = $this->sparkapi->searchSubscriber($search);
        // if object is returned then cast it to array
        if ($subscriber !== false) $subscriber = (array) $subscriber;
        return $subscriber;
    }
}
