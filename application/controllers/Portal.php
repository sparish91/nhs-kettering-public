<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Portal controller, presents portal to user when redirected from Flint.
 *
 * @package    Spark
 * @copyright  2003-2016 WifiSpark Limited. All rights reserved.
 * @license    http://www.wifispark.com/license/spark-1.00.txt  SPARK License 1.00
 * @version    GIT: $Id$
 */

class Portal extends CI_Controller
{

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');

        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('session');

        $this->load->model('subscriber');
    }

    /**
     * start() - the default controller entrance point
     */
    public function start() {
        log_message('debug', '****USER JOURNEY**** : redirected to by Flint');
        // get the device MAC if provided
        if (null != $this->input->get('MAC')) {
            $_SESSION['mac'] = $this->input->get('MAC');
        } else {
            // if MAC wasn't provided, check we know about it already else bail
            if (!isset($_SESSION['mac'])) {
                log_message('error', 'No MAC address known!');
                show_error('We could not determine your device ID, please try again later.', 400);
            }
        }
        $remote =  $_SERVER['REMOTE_ADDR'];
        $_SESSION['rip'] = $remote;
        $hotspots = (array)$this->config->item('settings_parsed')->hotspot_details;

        if (!array_key_exists($remote, $hotspots) && !isset($_GET['hotspot_id'])) {
            log_message('error', 'Hotspot not known');
            show_error('You must be connected to the wireless network to access this portal', 400);
        }

        // get the destination address
        if (null != $this->input->get('OS')) $_SESSION['os'] = $this->input->get('OS');
        $subscriber = $this->subscriber->getByMAC($_SESSION['mac']);
        if ($subscriber === false) {
            // we don't know about this MAC, show registration form
            log_message('debug', '****USER JOURNEY**** : new user, show registration form');
            $this->view();
        } else {
            log_message('debug', '****USER JOURNEY**** : last registered '.$subscriber['user_def02']);
            // check when the subscriber registered
            $date_parts = explode('-', $subscriber['user_def02']);      // registration date
            $now = date('Y-m-d H:i:s');
            $timestamp = mktime($date_parts[3], $date_parts[4], $date_parts[5], $date_parts[1], $date_parts[2], $date_parts[0]);
            // add on time granted to validated users
            $timestamp += $this->config->item('settings_parsed')->validated_access_time;
            $validation_expires = date('Y-m-d H:i:s', $timestamp);
            log_message('debug', '****USER JOURNEY**** : expires : ' . $validation_expires . ' now : ' .$now);
            if ($now >= $validation_expires) {
                // subscriber is too old, make them reregister
                log_message('debug', '****USER JOURNEY**** : user must re-register');
                $this->reset(true);
            } else {
                if ('1' == $subscriber['user_def03']) {                        // validated flag
                    // log_message('debug', '****USER JOURNEY**** : show welcome back message');
                    // $this->view('welcome_back', $subscriber['first_name']);
                    $this->view('welcome_back');
                } else {
                    log_message('debug', '****USER JOURNEY**** : show validation required message');
                    $this->view('validation_required');
                }
            }
        }
    }

    /**
     * view() - display a view
     */
    public function view($page = 'wifi_registration', $user = 'you') {
        if ( ! file_exists(APPPATH.'/views/portal/'.$page.'.php')) {
                // Whoops, we don't have a page for that!
                show_404();
        }

        // replace underscore with spaces and capitalize the first letter of each word
        $data['title'] = ucwords(str_replace('_', ' ', $page));
        $data['user'] = ucfirst(strtolower($user));
        $data['landing_page'] = $this->config->item('settings_parsed')->landing_page;
        $data['download_speed'] = round($this->config->item('settings_parsed')->bandwidth->down / 1024, 2) . 'Mb';
        $data['validation_minutes'] = round($this->config->item('settings_parsed')->validation_access_time / 60);
        $data['validated_days'] = round($this->config->item('settings_parsed')->validated_access_time / 60 / 60 / 24, 2);
        $data['page'] = $page;

        $this->load->view('templates/header', $data);
        $this->load->view('portal/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    /**
     * register() - entrance point for registration form submit
     */
    public function register() {
        // generate validation rule set
        $validation_config = array();
        $form_items = $this->config->item('settings_parsed')->form;
        foreach ($form_items as $form_item) {
            if ($form_item->server_validation_rules) {
                $rules = (($form_item->required)?'required|':'') . $form_item->server_validation_rules;
                $validation_config[] = array(
                    'field' => $form_item->name,
                    'label' => $form_item->server_validation_label,
                    'rules' => $rules
                );
            }
        }
        $this->form_validation->set_rules($validation_config);
        // log_message('DEBUG','VALIDATION: ' . $this->form_validation->run());
        // if (false == $this->form_validation->run()) {
        //     // form didn't validate, so show the registration page
        //     $this->view();
        // } else {
            $subscriber = $this->input->post();

            $subscriber['id'] = $this->subscriber->create($subscriber);

            if ($subscriber['id'] !== false) {
                // $this->sendEmail($subscriber);
                $this->view('success');
            } else {
                $this->view('error');
            }
        // }
    }

    /**
     * proceed() - when user clicks "Proceed to internet" button after being welcomed back
     */
    public function proceed() {
        // $rip = (isset($_SESSION['rip']))?$_SESSION['rip']:$this->config->item('settings_parsed')->default_hotspot_ip;
        // $this->subscriber->reactivate($rip, $_SESSION['mac']);
        $redirect = $this->config->item('settings_parsed')->landing_page;
        $this->iosredirect($redirect,'reactivate');
    }

    /**
     * connect() - when user clicks "Proceed to internet" button after having validated themselves
     */
    public function connect($mac) {
        $rip = (isset($_SESSION['rip']))?$_SESSION['rip']:$this->config->item('settings_parsed')->default_hotspot_ip;
        $this->subscriber->reactivate($rip, $mac);

        $redirect = $this->config->item('settings_parsed')->landing_page;
        if ('' == $redirect) {
            $redirect = $_SESSION['os'];
        }
        redirect($redirect);
    }

    /**
     * go() - when a user is validated, this function will enable any link that appears on that page to activate their session rather than the standard 'Proceed()' which takes to the default landing page.
     * The url that is passed in to this function should use ASCII encoding for characters, else it will fail. e.g.  href="/go/https%3A%2F%2Fwww.test.com%2Ftest" rather than: href="/go/https://test.com/test"
     */
    public function go($url) {
        $rip = isset($_SESSION['rip'])?$_SESSION['rip']:$this->config->item('settings_parsed')->default_hotspot_ip;
        $this->subscriber->reactivate($rip, $_SESSION['mac']);
        $url = urldecode($url);
        redirect($url);
    }

    /**
     * reset($expired) - update the subscriber name so that they can re-register
     * $expired boolean - reason for reset true = expired, false = user triggered
     */
    public function reset($expired = false) {
        $subscriber = $this->subscriber->getByMAC($_SESSION['mac']);

        $analyticsData = array();
        $analyticsData['type'] = 'Portal';
        $analyticsData['view'] = 'ResetUser';
        $analyticsData['reason'] = $expired?'expired':'user_reset';
        $analyticsData['device_mac_address'] = $subscriber['user_def01'];
        $analyticsData['view_valid'] = true;
        $analyticsData['doing_ajax'] = false;
        $analyticsData['username'] = $subscriber['login'];
        $analyticsData['user_id'] = $subscriber['id'];

        $this->subscriber->analytics($analyticsData);

        $this->subscriber->deleteMAC($subscriber['id']);

        $this->view();
    }

    /**
     * resend() - send out email again
     */
    public function resend() {
        $subscriber = $this->subscriber->getByMAC($_SESSION['mac']);
        $subscriber['mac'] = $subscriber['user_def01'];                 // copy mac address to where sendEmail() expects
        $this->sendEmail($subscriber);
        $this->view('resent');
    }

    /**
     * verify() - check email the user wants the email resent to matches what they registered
     */
    public function verify() {
        $subscriber = $this->subscriber->getByMAC($_SESSION['mac']);
        if ($subscriber['email'] == $this->input->post('email')) {
            $subscriber['mac'] = $subscriber['user_def01'];             // copy mac address to where sendEmail() expects
            $this->sendEmail($subscriber);
            $this->view('resent');
        } else {
            $this->view('incorrect_email');
        }
    }

     /**
     * pageview() - record a pageview in splunk
     */
    public function pageview($page) {
        log_message('debug', 'Pageview : ' . $page);

        // More info about the fields can be found at:
        // http://new-wiki/doku.php?id=development:splunk&s[]=analytics
        $analyticsData = array();
        $analyticsData['type'] = 'Portal';
        $analyticsData['view'] = 'PortalView';
        $analyticsData['page'] = $page;
        $analyticsData['device_mac_address'] = $_SESSION['mac'];
        $analyticsData['view_valid'] = true;
        $analyticsData['doing_ajax'] = false;
        // add in subscriber details for certain pages
        $subscriberDetailsPages = ['thank_you','welcome_back','validation_required'];
        if (array_search($page, $subscriberDetailsPages) !== false) {
            $analyticsData['subscriber'] = $this->subscriber->getByMAC($_SESSION['mac']);
        }

        $this->subscriber->analytics($analyticsData);
    }

    /**
     * sendEmail() - send email to user
     */
    private function sendEmail($subscriber) {
        $config['mailtype'] = 'html';
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";

        $this->email->initialize($config);
        $this->email->from($this->config->item('settings_parsed')->welcome_email->from);
        $this->email->to($subscriber['email']);
        $this->email->subject($this->config->item('settings_parsed')->welcome_email->subject);
        $logo_path = FCPATH . 'assets/images/logo.png';
        $this->email->attach($logo_path);
        $subscriber['logo_cid'] = $this->email->attachment_cid($logo_path);

        if ($this->config->item('settings_parsed')->friendly_wifi) {
            $friendly_path = FCPATH . 'assets/images/friendly_wifi.png';
            $this->email->attach($friendly_path);
            $subscriber['friendly_cid'] = $this->email->attachment_cid($friendly_path);
        }

        $mac_parts = str_split(strtolower($subscriber['mac']), 6);

        // convert dotdecimal IP address to hex
        $ip = dechex(ip2long($_SESSION['rip']));

        // construct checksum
        $checksum = md5($mac_parts[0] . $mac_parts[1] . $subscriber['id'] . $ip . ' a little salt!');

        $subscriber['link'] = $this->config->base_url() . 'validate/' . $mac_parts[0] . '/' . $subscriber['id'] . '/' . $mac_parts[1] . '/' . $ip . '/' . $checksum;

        $subscriber['friendly_wifi'] = $this->config->item('settings_parsed')->friendly_wifi;
        $mesg = $this->load->view('templates/email', $subscriber, true);

        $this->email->message($mesg);
        $this->email->send();
    }

    /**
     * iosredirect() - acivate after proceed button clicked
     */
    public function iosredirect($url, $status = null) {
      $rip = (isset($_SESSION['rip']))?$_SESSION['rip']:$this->config->item('settings_parsed')->default_hotspot_ip;
      $subscriber = $this->subscriber->getByMAC($_SESSION['mac']);
      $newUrl = urldecode($url);
      $_SESSION['url'] = $newUrl;
      if ($subscriber['id'] !== false) {
        if ($status === "reactivate") {
          $this->subscriber->reactivate($rip, $_SESSION['mac']);
        } else {
          $this->subscriber->activate($subscriber['id'], $_SESSION['mac'], $this->config->item('settings_parsed')->validated_access_time);
        }
        $this->view('iosredirect');
      }
    }
}
