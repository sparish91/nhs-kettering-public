<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Email controller, deals with inbound link from validation email.
 *
 * @package    Spark
 * @copyright  2003-2016 WifiSpark Limited. All rights reserved.
 * @license    http://www.wifispark.com/license/spark-1.00.txt  SPARK License 1.00
 * @version    GIT: $Id$
 */

class Email extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->library('session');
        $this->load->model('subscriber');
    }

    function validate($mac_part1, $subscriber_id, $mac_part2, $ip, $checksum)
    {
        // construct checksum as per application/controllers/Portal.php->sendEmail()
        $check = md5($mac_part1 . $mac_part2 . $subscriber_id . $ip . ' a little salt!');
        if ($check != $checksum) {
            show_error('Please try again or contact support if the problem persists.', 400, 'Malformed validation link');
        }

        $mac = strtoupper($mac_part1 . $mac_part2);
        $_SESSION['mac'] = $mac;

        // convert IP from hex to dot-decimal
        $_SESSION['rip'] = long2ip(hexdec($ip));

        $data['title'] = 'Thank you';

        $subscriber = $this->subscriber->validate($subscriber_id);
        $this->subscriber->provision($subscriber_id, $mac);

        if (isset($subscriber->error)) {
            show_error('Something went wrong, please try again.');
        }

        $data['user'] = $subscriber->first_name;
        $data['subscriber_id'] = $subscriber_id;
        $data['mac'] = $mac;
        $data['landing_page'] = $this->config->item('settings_parsed')->landing_page;
        $data['download_speed'] = round($this->config->item('settings_parsed')->bandwidth->down / 1024, 2) . 'Mb';
        $data['validation_minutes'] = round($this->config->item('settings_parsed')->validation_access_time / 60);
        $data['validated_days'] = round($this->config->item('settings_parsed')->validated_access_time / 60 / 60 / 24, 2);
        $data['page'] = 'thank_you';

        $this->load->view('templates/header', $data);
        $this->load->view('portal/thank_you', $data);
        $this->load->view('templates/footer', $data);
    }
}
