/**
 * Javascript validation for the Portals
 * 
 * Use http://bootstrapvalidator.com/ to validate each of the form fields. Each input
 * element can have zero or more validators applied to check validity. These are defined
 * as HTML5 data attributes to each input along with any error message for I18N.
 * 
 * Form submission is performed via AJAX to avoid page refreshes.
 *
 * @package    Spark
 * @copyright  2003-2014 WifiSpark Limited. All rights reserved.
 * @license    http://www.wifispark.com/license/spark-1.00.txt  SPARK License 1.00
 */

$(document).ready(function() {
    
    // Apply BootstrapValidator plugin to each Portal form
    $('form.portal-form').bootstrapValidator({
        message: 'This value is not valid',
        trigger: 'blur',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        // Field validators are defined via HTML5 attributes along with
        // the error messages for I18N apart from gender as we need to
        // check something has been selected.
        fields: {
            gender: {
                validators: {
                    callback: {
                        callback: function(value, validator) {
                            // Get the selected options
                            var options = validator.getFieldElements('gender').val();
                            return (options != 0);
                        }
                    }
                }
            },
        }
    }).on('error.validator.bv', function(e, data) {
        // Triggered on field validation failure
        // $(e.target)    --> The form instance
        // data.field     --> The field name
        // data.element   --> The field element
        // data.validator --> The validator name
        
        // If there is an invalid field, hide the AJAX response error
        // ready to show the validators error. Only 1 error at a time.
        $('#form-error-message').val('')
                                .addClass('hidden');
        
        // Only show one validator error message at a time.
        // A field may have multiple validators, don't swamp the user.
        data.element
            .data('bv.messages')
            // Hide all the messages
            .find('.help-block[data-bv-for="' + data.field + '"]').hide()
            // Show only message associated with current validator
            .filter('[data-bv-validator="' + data.validator + '"]').show();
        
    }).on('success.validator.bv', function(e, data) {
        // Triggered on field validation success
    }).on('success.form.bv', function(e) {
        // Triggered on form validation success
        
        /*
         * Handle clicking Submit when the fields are considered
         * valid. Once submitted we either proceed to the Portal_Process_*
         * view or display an error.
         *
         * WARNING: Do not use the properties of a form, such as submit, reset, length, method to set
         * the name or id attribute of form, field elements. Name conflicts can cause the problem.
         * http://bootstrapvalidator.com/getting-started/#name-conflict
         *
         */
        
        // Get the form instance
        var $form = $(e.target);
        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');
        
        if (!bv.getSubmitButton()) {
            // Submit not clicked - avoid issue when calling bv.isValid() automatically submitting form.
            
            return false;
        }
        
        
        // If the form supports AJAX, otherwise normal POST
        if ($form.data().hasOwnProperty("ajax")) {
            
            // Prevent form submission for AJAX requests
            //e.preventDefault();
            // Use Ajax to submit form data
             /*
            $.post('https://starbucks-test.wifispark.net/index_submit.php', $form.serialize(), function(result) {
                
                if ( result.valid ) {
                    // Redirect to process view to finish logging in
                    window.location.href = result.redirect_next_step;
                } else {
                    
                    // We may have individual field errors, or a form error
                    // that relates to the whole form
                    if ( "field_errors" in result && result.field_errors ) {
                        for (var prop in result.field_errors) {
                            if (result.field_errors.hasOwnProperty(prop)) {
                                // Each error div is named with the suffix -$field_name
                                var errors = $('#form-error-message' + '-' + prop);
                                $(errors).closest('.form-group').removeClass('has-success')
                                                                .addClass('has-error')
                                                                .find('i.form-control-feedback')
                                                                .removeClass('glyphicon-ok')
                                                                .addClass('glyphicon-remove');
                                $(errors).html(result.field_errors[prop]).removeClass('hidden');
                            }
                        }
                    } else {
                        // Display form errors within a form-group
                        var errors = $('#form-error-message');
                        $(errors).closest('.form-group').removeClass('has-success')
                                                        .addClass('has-error')
                                                        .find('i.form-control-feedback')
                                                        .removeClass('glyphicon-ok')
                                                        .addClass('glyphicon-remove');
                        $(errors).html(result.message).removeClass('hidden');
                    }
                    
                    // For subscribers, display a Buy Time option if they have run out
                    if ( "no_time_left" in result && result.no_time_left ) {
                        $('#buy_more_time').removeClass('hidden');
                    }
                }
                
            }, 'json');
	*/
        }
    });
    
    // Clear the current input error field if the red X is clicked on the feedback icons.
    $('div.form-group').on('click', 'i.glyphicon-remove', function() {
        var field = '#' + $(this).data('bv-icon-for');
        if ($(field).is('input')) {
            $(field).val('').focus();
        }
    });
    
});
