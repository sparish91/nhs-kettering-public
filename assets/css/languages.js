import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "bfh-flag-AD": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1921px 0"
    },
    "bfh-flag-AE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1904px 0"
    },
    "bfh-flag-AF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3689px 0"
    },
    "bfh-flag-AG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-34px 0"
    },
    "bfh-flag-AI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-51px 0"
    },
    "bfh-flag-AL": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-68px 0"
    },
    "bfh-flag-AM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-85px 0"
    },
    "bfh-flag-AN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-102px 0"
    },
    "bfh-flag-AO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-119px 0"
    },
    "bfh-flag-AQ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-136px 0"
    },
    "bfh-flag-AR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-153px 0"
    },
    "bfh-flag-AS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-170px 0"
    },
    "bfh-flag-AT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-187px 0"
    },
    "bfh-flag-AU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-204px 0"
    },
    "bfh-flag-AW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-221px 0"
    },
    "bfh-flag-AX": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-238px 0"
    },
    "bfh-flag-AZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-255px 0"
    },
    "bfh-flag-BA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-272px 0"
    },
    "bfh-flag-BB": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-289px 0"
    },
    "bfh-flag-BD": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-306px 0"
    },
    "bfh-flag-BE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-323px 0"
    },
    "bfh-flag-BG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-340px 0"
    },
    "bfh-flag-BH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-357px 0"
    },
    "bfh-flag-BI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-374px 0"
    },
    "bfh-flag-BJ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-391px 0"
    },
    "bfh-flag-BL": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-408px 0"
    },
    "bfh-flag-BM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-425px 0"
    },
    "bfh-flag-BN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-442px 0"
    },
    "bfh-flag-BO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-459px 0"
    },
    "bfh-flag-BR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-476px 0"
    },
    "bfh-flag-BS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-493px 0"
    },
    "bfh-flag-BT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-510px 0"
    },
    "bfh-flag-BW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-527px 0"
    },
    "bfh-flag-BY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-544px 0"
    },
    "bfh-flag-BZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-561px 0"
    },
    "bfh-flag-CA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-578px 0"
    },
    "bfh-flag-CD": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-595px 0"
    },
    "bfh-flag-CF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-612px 0"
    },
    "bfh-flag-CG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-629px 0"
    },
    "bfh-flag-CH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-646px 0"
    },
    "bfh-flag-CI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-663px 0"
    },
    "bfh-flag-CL": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-680px 0"
    },
    "bfh-flag-CM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-697px 0"
    },
    "bfh-flag-CN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-714px 0"
    },
    "bfh-flag-CO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-731px 0"
    },
    "bfh-flag-CR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-748px 0"
    },
    "bfh-flag-CV": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-765px 0"
    },
    "bfh-flag-CY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-782px 0"
    },
    "bfh-flag-CZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-799px 0"
    },
    "bfh-flag-DJ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-816px 0"
    },
    "bfh-flag-DK": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-833px 0"
    },
    "bfh-flag-DM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-850px 0"
    },
    "bfh-flag-DO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-867px 0"
    },
    "bfh-flag-DZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-884px 0"
    },
    "bfh-flag-EC": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-901px 0"
    },
    "bfh-flag-EE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-918px 0"
    },
    "bfh-flag-EG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-935px 0"
    },
    "bfh-flag-EH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-952px 0"
    },
    "bfh-flag-ER": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-969px 0"
    },
    "bfh-flag-ES": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-986px 0"
    },
    "bfh-flag-ET": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1003px 0"
    },
    "bfh-flag-EU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1020px 0"
    },
    "bfh-flag-FI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1037px 0"
    },
    "bfh-flag-FJ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1054px 0"
    },
    "bfh-flag-FK": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1071px 0"
    },
    "bfh-flag-FM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1088px 0"
    },
    "bfh-flag-FO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1105px 0"
    },
    "bfh-flag-FR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-FX": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-GF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-GP": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-MQ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-NC": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-PF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-PM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-RE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-TF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-WF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1122px 0"
    },
    "bfh-flag-GA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1139px 0"
    },
    "bfh-flag-GB": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1156px 0"
    },
    "bfh-flag-GD": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1173px 0"
    },
    "bfh-flag-GE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1190px 0"
    },
    "bfh-flag-GG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1207px 0"
    },
    "bfh-flag-GH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1224px 0"
    },
    "bfh-flag-GL": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1241px 0"
    },
    "bfh-flag-GM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1258px 0"
    },
    "bfh-flag-GN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1275px 0"
    },
    "bfh-flag-GQ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1292px 0"
    },
    "bfh-flag-GR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1309px 0"
    },
    "bfh-flag-GS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1326px 0"
    },
    "bfh-flag-GT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1343px 0"
    },
    "bfh-flag-GU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1360px 0"
    },
    "bfh-flag-GW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1377px 0"
    },
    "bfh-flag-GY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1394px 0"
    },
    "bfh-flag-HK": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1411px 0"
    },
    "bfh-flag-HN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1428px 0"
    },
    "bfh-flag-HR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1445px 0"
    },
    "bfh-flag-HT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1462px 0"
    },
    "bfh-flag-HU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1479px 0"
    },
    "bfh-flag-ID": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1496px 0"
    },
    "bfh-flag-IE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1513px 0"
    },
    "bfh-flag-IL": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1530px 0"
    },
    "bfh-flag-IM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1547px 0"
    },
    "bfh-flag-IN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1564px 0"
    },
    "bfh-flag-IQ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1581px 0"
    },
    "bfh-flag-IS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1598px 0"
    },
    "bfh-flag-IT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1615px 0"
    },
    "bfh-flag-JE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1632px 0"
    },
    "bfh-flag-JM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1649px 0"
    },
    "bfh-flag-JO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1666px 0"
    },
    "bfh-flag-JP": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1683px 0"
    },
    "bfh-flag-KE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1700px 0"
    },
    "bfh-flag-KG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1717px 0"
    },
    "bfh-flag-KH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1734px 0"
    },
    "bfh-flag-KI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1751px 0"
    },
    "bfh-flag-KM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1768px 0"
    },
    "bfh-flag-KN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1785px 0"
    },
    "bfh-flag-KP": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1802px 0"
    },
    "bfh-flag-KR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1819px 0"
    },
    "bfh-flag-KV": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1836px 0"
    },
    "bfh-flag-KW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1853px 0"
    },
    "bfh-flag-KY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1870px 0"
    },
    "bfh-flag-LA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1887px 0"
    },
    "bfh-flag-LC": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "0 0"
    },
    "bfh-flag-LK": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-17px 0"
    },
    "bfh-flag-LR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1938px 0"
    },
    "bfh-flag-LS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1955px 0"
    },
    "bfh-flag-LT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1972px 0"
    },
    "bfh-flag-LU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-1989px 0"
    },
    "bfh-flag-LV": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2006px 0"
    },
    "bfh-flag-LY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2023px 0"
    },
    "bfh-flag-MA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2040px 0"
    },
    "bfh-flag-ME": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2057px 0"
    },
    "bfh-flag-MG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2074px 0"
    },
    "bfh-flag-MH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2091px 0"
    },
    "bfh-flag-ML": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2108px 0"
    },
    "bfh-flag-MM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2125px 0"
    },
    "bfh-flag-MP": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2142px 0"
    },
    "bfh-flag-MR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2159px 0"
    },
    "bfh-flag-MS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2176px 0"
    },
    "bfh-flag-MT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2193px 0"
    },
    "bfh-flag-MU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2210px 0"
    },
    "bfh-flag-MV": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2227px 0"
    },
    "bfh-flag-MW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2244px 0"
    },
    "bfh-flag-MZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2261px 0"
    },
    "bfh-flag-NA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2278px 0"
    },
    "bfh-flag-NE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2295px 0"
    },
    "bfh-flag-NF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2312px 0"
    },
    "bfh-flag-NG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2329px 0"
    },
    "bfh-flag-NI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2346px 0"
    },
    "bfh-flag-NL": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2363px 0"
    },
    "bfh-flag-NO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2380px 0"
    },
    "bfh-flag-NP": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2397px 0"
    },
    "bfh-flag-NR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2414px 0"
    },
    "bfh-flag-NZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2431px 0"
    },
    "bfh-flag-OM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2448px 0"
    },
    "bfh-flag-PA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2465px 0"
    },
    "bfh-flag-PE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2482px 0"
    },
    "bfh-flag-PG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2499px 0"
    },
    "bfh-flag-PH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2516px 0"
    },
    "bfh-flag-PK": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2533px 0"
    },
    "bfh-flag-PL": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2550px 0"
    },
    "bfh-flag-PN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2567px 0"
    },
    "bfh-flag-PS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2584px 0"
    },
    "bfh-flag-PT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2601px 0"
    },
    "bfh-flag-PW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2618px 0"
    },
    "bfh-flag-PY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2635px 0"
    },
    "bfh-flag-QA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2652px 0"
    },
    "bfh-flag-RS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2669px 0"
    },
    "bfh-flag-RU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2686px 0"
    },
    "bfh-flag-RW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2703px 0"
    },
    "bfh-flag-SA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2720px 0"
    },
    "bfh-flag-SB": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2737px 0"
    },
    "bfh-flag-SC": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2754px 0"
    },
    "bfh-flag-SD": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2771px 0"
    },
    "bfh-flag-SE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2788px 0"
    },
    "bfh-flag-SG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2805px 0"
    },
    "bfh-flag-SH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2822px 0"
    },
    "bfh-flag-SI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2839px 0"
    },
    "bfh-flag-SK": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2856px 0"
    },
    "bfh-flag-SM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2873px 0"
    },
    "bfh-flag-SN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2890px 0"
    },
    "bfh-flag-SO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2907px 0"
    },
    "bfh-flag-SR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2924px 0"
    },
    "bfh-flag-SS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2941px 0"
    },
    "bfh-flag-ST": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2958px 0"
    },
    "bfh-flag-SV": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2975px 0"
    },
    "bfh-flag-SY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-2992px 0"
    },
    "bfh-flag-SZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3009px 0"
    },
    "bfh-flag-TC": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3026px 0"
    },
    "bfh-flag-TD": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3043px 0"
    },
    "bfh-flag-TG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3060px 0"
    },
    "bfh-flag-TH": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3077px 0"
    },
    "bfh-flag-TJ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3094px 0"
    },
    "bfh-flag-TM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3111px 0"
    },
    "bfh-flag-TN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3128px 0"
    },
    "bfh-flag-TP": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3145px 0"
    },
    "bfh-flag-TR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3162px 0"
    },
    "bfh-flag-TT": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3179px 0"
    },
    "bfh-flag-TV": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3196px 0"
    },
    "bfh-flag-TW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3213px 0"
    },
    "bfh-flag-TZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3230px 0"
    },
    "bfh-flag-UA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3247px 0"
    },
    "bfh-flag-UG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3264px 0"
    },
    "bfh-flag-US": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3281px 0"
    },
    "bfh-flag-UY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3298px 0"
    },
    "bfh-flag-UZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3315px 0"
    },
    "bfh-flag-VC": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3332px 0"
    },
    "bfh-flag-VE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3349px 0"
    },
    "bfh-flag-VG": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3366px 0"
    },
    "bfh-flag-VI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3383px 0"
    },
    "bfh-flag-VN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3400px 0"
    },
    "bfh-flag-VU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3417px 0"
    },
    "bfh-flag-WS": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3434px 0"
    },
    "bfh-flag-YE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3451px 0"
    },
    "bfh-flag-ZA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3468px 0"
    },
    "bfh-flag-ZM": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3485px 0"
    },
    "bfh-flag-BF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3502px 0"
    },
    "bfh-flag-CU": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3519px 0"
    },
    "bfh-flag-DE": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3536px 0"
    },
    "bfh-flag-IR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3553px 0"
    },
    "bfh-flag-KZ": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3570px 0"
    },
    "bfh-flag-LB": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3587px 0"
    },
    "bfh-flag-LI": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3604px 0"
    },
    "bfh-flag-MC": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3621px 0"
    },
    "bfh-flag-MD": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3638px 0"
    },
    "bfh-flag-MK": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3655px 0"
    },
    "bfh-flag-MN": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3672px 0"
    },
    "bfh-flag-MO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3706px 0"
    },
    "bfh-flag-MX": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3723px 0"
    },
    "bfh-flag-MY": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3740px 0"
    },
    "bfh-flag-PR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3757px 0"
    },
    "bfh-flag-RO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3774px 0"
    },
    "bfh-flag-SL": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3791px 0"
    },
    "bfh-flag-TO": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3808px 0"
    },
    "bfh-flag-VA": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3825px 0"
    },
    "bfh-flag-ZW": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-countries.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-3842px 0"
    },
    "bfh-flag-AD:empty": {
        "width": 16
    },
    "bfh-flag-AE:empty": {
        "width": 16
    },
    "bfh-flag-AF:empty": {
        "width": 16
    },
    "bfh-flag-AG:empty": {
        "width": 16
    },
    "bfh-flag-AI:empty": {
        "width": 16
    },
    "bfh-flag-AL:empty": {
        "width": 16
    },
    "bfh-flag-AM:empty": {
        "width": 16
    },
    "bfh-flag-AN:empty": {
        "width": 16
    },
    "bfh-flag-AO:empty": {
        "width": 16
    },
    "bfh-flag-AQ:empty": {
        "width": 16
    },
    "bfh-flag-AR:empty": {
        "width": 16
    },
    "bfh-flag-AS:empty": {
        "width": 16
    },
    "bfh-flag-AT:empty": {
        "width": 16
    },
    "bfh-flag-AU:empty": {
        "width": 16
    },
    "bfh-flag-AW:empty": {
        "width": 16
    },
    "bfh-flag-AX:empty": {
        "width": 16
    },
    "bfh-flag-AZ:empty": {
        "width": 16
    },
    "bfh-flag-BA:empty": {
        "width": 16
    },
    "bfh-flag-BB:empty": {
        "width": 16
    },
    "bfh-flag-BD:empty": {
        "width": 16
    },
    "bfh-flag-BE:empty": {
        "width": 16
    },
    "bfh-flag-BG:empty": {
        "width": 16
    },
    "bfh-flag-BH:empty": {
        "width": 16
    },
    "bfh-flag-BI:empty": {
        "width": 16
    },
    "bfh-flag-BJ:empty": {
        "width": 16
    },
    "bfh-flag-BL:empty": {
        "width": 16
    },
    "bfh-flag-BM:empty": {
        "width": 16
    },
    "bfh-flag-BN:empty": {
        "width": 16
    },
    "bfh-flag-BO:empty": {
        "width": 16
    },
    "bfh-flag-BR:empty": {
        "width": 16
    },
    "bfh-flag-BS:empty": {
        "width": 16
    },
    "bfh-flag-BT:empty": {
        "width": 16
    },
    "bfh-flag-BW:empty": {
        "width": 16
    },
    "bfh-flag-BY:empty": {
        "width": 16
    },
    "bfh-flag-BZ:empty": {
        "width": 16
    },
    "bfh-flag-CA:empty": {
        "width": 16
    },
    "bfh-flag-CD:empty": {
        "width": 16
    },
    "bfh-flag-CF:empty": {
        "width": 16
    },
    "bfh-flag-CG:empty": {
        "width": 16
    },
    "bfh-flag-CH:empty": {
        "width": 16
    },
    "bfh-flag-CI:empty": {
        "width": 16
    },
    "bfh-flag-CL:empty": {
        "width": 16
    },
    "bfh-flag-CM:empty": {
        "width": 16
    },
    "bfh-flag-CN:empty": {
        "width": 16
    },
    "bfh-flag-CO:empty": {
        "width": 16
    },
    "bfh-flag-CR:empty": {
        "width": 16
    },
    "bfh-flag-CV:empty": {
        "width": 16
    },
    "bfh-flag-CY:empty": {
        "width": 16
    },
    "bfh-flag-CZ:empty": {
        "width": 16
    },
    "bfh-flag-DJ:empty": {
        "width": 16
    },
    "bfh-flag-DK:empty": {
        "width": 16
    },
    "bfh-flag-DM:empty": {
        "width": 16
    },
    "bfh-flag-DO:empty": {
        "width": 16
    },
    "bfh-flag-DZ:empty": {
        "width": 16
    },
    "bfh-flag-EC:empty": {
        "width": 16
    },
    "bfh-flag-EE:empty": {
        "width": 16
    },
    "bfh-flag-EG:empty": {
        "width": 16
    },
    "bfh-flag-EH:empty": {
        "width": 16
    },
    "bfh-flag-ER:empty": {
        "width": 16
    },
    "bfh-flag-ES:empty": {
        "width": 16
    },
    "bfh-flag-ET:empty": {
        "width": 16
    },
    "bfh-flag-EU:empty": {
        "width": 16
    },
    "bfh-flag-FI:empty": {
        "width": 16
    },
    "bfh-flag-FJ:empty": {
        "width": 16
    },
    "bfh-flag-FK:empty": {
        "width": 16
    },
    "bfh-flag-FM:empty": {
        "width": 16
    },
    "bfh-flag-FO:empty": {
        "width": 16
    },
    "bfh-flag-FR:empty": {
        "width": 16
    },
    "bfh-flag-FX:empty": {
        "width": 16
    },
    "bfh-flag-GF:empty": {
        "width": 16
    },
    "bfh-flag-GP:empty": {
        "width": 16
    },
    "bfh-flag-MQ:empty": {
        "width": 16
    },
    "bfh-flag-NC:empty": {
        "width": 16
    },
    "bfh-flag-PF:empty": {
        "width": 16
    },
    "bfh-flag-PM:empty": {
        "width": 16
    },
    "bfh-flag-RE:empty": {
        "width": 16
    },
    "bfh-flag-TF:empty": {
        "width": 16
    },
    "bfh-flag-WF:empty": {
        "width": 16
    },
    "bfh-flag-GA:empty": {
        "width": 16
    },
    "bfh-flag-GB:empty": {
        "width": 16
    },
    "bfh-flag-GD:empty": {
        "width": 16
    },
    "bfh-flag-GE:empty": {
        "width": 16
    },
    "bfh-flag-GG:empty": {
        "width": 16
    },
    "bfh-flag-GH:empty": {
        "width": 16
    },
    "bfh-flag-GL:empty": {
        "width": 16
    },
    "bfh-flag-GM:empty": {
        "width": 16
    },
    "bfh-flag-GN:empty": {
        "width": 16
    },
    "bfh-flag-GQ:empty": {
        "width": 16
    },
    "bfh-flag-GR:empty": {
        "width": 16
    },
    "bfh-flag-GS:empty": {
        "width": 16
    },
    "bfh-flag-GT:empty": {
        "width": 16
    },
    "bfh-flag-GU:empty": {
        "width": 16
    },
    "bfh-flag-GW:empty": {
        "width": 16
    },
    "bfh-flag-GY:empty": {
        "width": 16
    },
    "bfh-flag-HK:empty": {
        "width": 16
    },
    "bfh-flag-HN:empty": {
        "width": 16
    },
    "bfh-flag-HR:empty": {
        "width": 16
    },
    "bfh-flag-HT:empty": {
        "width": 16
    },
    "bfh-flag-HU:empty": {
        "width": 16
    },
    "bfh-flag-ID:empty": {
        "width": 16
    },
    "bfh-flag-IE:empty": {
        "width": 16
    },
    "bfh-flag-IL:empty": {
        "width": 16
    },
    "bfh-flag-IM:empty": {
        "width": 16
    },
    "bfh-flag-IN:empty": {
        "width": 16
    },
    "bfh-flag-IQ:empty": {
        "width": 16
    },
    "bfh-flag-IS:empty": {
        "width": 16
    },
    "bfh-flag-IT:empty": {
        "width": 16
    },
    "bfh-flag-JE:empty": {
        "width": 16
    },
    "bfh-flag-JM:empty": {
        "width": 16
    },
    "bfh-flag-JO:empty": {
        "width": 16
    },
    "bfh-flag-JP:empty": {
        "width": 16
    },
    "bfh-flag-KE:empty": {
        "width": 16
    },
    "bfh-flag-KG:empty": {
        "width": 16
    },
    "bfh-flag-KH:empty": {
        "width": 16
    },
    "bfh-flag-KI:empty": {
        "width": 16
    },
    "bfh-flag-KM:empty": {
        "width": 16
    },
    "bfh-flag-KN:empty": {
        "width": 16
    },
    "bfh-flag-KP:empty": {
        "width": 16
    },
    "bfh-flag-KR:empty": {
        "width": 16
    },
    "bfh-flag-KV:empty": {
        "width": 16
    },
    "bfh-flag-KW:empty": {
        "width": 16
    },
    "bfh-flag-KY:empty": {
        "width": 16
    },
    "bfh-flag-LA:empty": {
        "width": 16
    },
    "bfh-flag-LC:empty": {
        "width": 16
    },
    "bfh-flag-LK:empty": {
        "width": 16
    },
    "bfh-flag-LR:empty": {
        "width": 16
    },
    "bfh-flag-LS:empty": {
        "width": 16
    },
    "bfh-flag-LT:empty": {
        "width": 16
    },
    "bfh-flag-LU:empty": {
        "width": 16
    },
    "bfh-flag-LV:empty": {
        "width": 16
    },
    "bfh-flag-LY:empty": {
        "width": 16
    },
    "bfh-flag-MA:empty": {
        "width": 16
    },
    "bfh-flag-ME:empty": {
        "width": 16
    },
    "bfh-flag-MG:empty": {
        "width": 16
    },
    "bfh-flag-MH:empty": {
        "width": 16
    },
    "bfh-flag-ML:empty": {
        "width": 16
    },
    "bfh-flag-MM:empty": {
        "width": 16
    },
    "bfh-flag-MP:empty": {
        "width": 16
    },
    "bfh-flag-MR:empty": {
        "width": 16
    },
    "bfh-flag-MS:empty": {
        "width": 16
    },
    "bfh-flag-MT:empty": {
        "width": 16
    },
    "bfh-flag-MU:empty": {
        "width": 16
    },
    "bfh-flag-MV:empty": {
        "width": 16
    },
    "bfh-flag-MW:empty": {
        "width": 16
    },
    "bfh-flag-MZ:empty": {
        "width": 16
    },
    "bfh-flag-NA:empty": {
        "width": 16
    },
    "bfh-flag-NE:empty": {
        "width": 16
    },
    "bfh-flag-NF:empty": {
        "width": 16
    },
    "bfh-flag-NG:empty": {
        "width": 16
    },
    "bfh-flag-NI:empty": {
        "width": 16
    },
    "bfh-flag-NL:empty": {
        "width": 16
    },
    "bfh-flag-NO:empty": {
        "width": 16
    },
    "bfh-flag-NP:empty": {
        "width": 16
    },
    "bfh-flag-NR:empty": {
        "width": 16
    },
    "bfh-flag-NZ:empty": {
        "width": 16
    },
    "bfh-flag-OM:empty": {
        "width": 16
    },
    "bfh-flag-PA:empty": {
        "width": 16
    },
    "bfh-flag-PE:empty": {
        "width": 16
    },
    "bfh-flag-PG:empty": {
        "width": 16
    },
    "bfh-flag-PH:empty": {
        "width": 16
    },
    "bfh-flag-PK:empty": {
        "width": 16
    },
    "bfh-flag-PL:empty": {
        "width": 16
    },
    "bfh-flag-PN:empty": {
        "width": 16
    },
    "bfh-flag-PS:empty": {
        "width": 16
    },
    "bfh-flag-PT:empty": {
        "width": 16
    },
    "bfh-flag-PW:empty": {
        "width": 16
    },
    "bfh-flag-PY:empty": {
        "width": 16
    },
    "bfh-flag-QA:empty": {
        "width": 16
    },
    "bfh-flag-RS:empty": {
        "width": 16
    },
    "bfh-flag-RU:empty": {
        "width": 16
    },
    "bfh-flag-RW:empty": {
        "width": 16
    },
    "bfh-flag-SA:empty": {
        "width": 16
    },
    "bfh-flag-SB:empty": {
        "width": 16
    },
    "bfh-flag-SC:empty": {
        "width": 16
    },
    "bfh-flag-SD:empty": {
        "width": 16
    },
    "bfh-flag-SE:empty": {
        "width": 16
    },
    "bfh-flag-SG:empty": {
        "width": 16
    },
    "bfh-flag-SH:empty": {
        "width": 16
    },
    "bfh-flag-SI:empty": {
        "width": 16
    },
    "bfh-flag-SK:empty": {
        "width": 16
    },
    "bfh-flag-SM:empty": {
        "width": 16
    },
    "bfh-flag-SN:empty": {
        "width": 16
    },
    "bfh-flag-SO:empty": {
        "width": 16
    },
    "bfh-flag-SR:empty": {
        "width": 16
    },
    "bfh-flag-SS:empty": {
        "width": 16
    },
    "bfh-flag-ST:empty": {
        "width": 16
    },
    "bfh-flag-SV:empty": {
        "width": 16
    },
    "bfh-flag-SY:empty": {
        "width": 16
    },
    "bfh-flag-SZ:empty": {
        "width": 16
    },
    "bfh-flag-TC:empty": {
        "width": 16
    },
    "bfh-flag-TD:empty": {
        "width": 16
    },
    "bfh-flag-TG:empty": {
        "width": 16
    },
    "bfh-flag-TH:empty": {
        "width": 16
    },
    "bfh-flag-TJ:empty": {
        "width": 16
    },
    "bfh-flag-TM:empty": {
        "width": 16
    },
    "bfh-flag-TN:empty": {
        "width": 16
    },
    "bfh-flag-TP:empty": {
        "width": 16
    },
    "bfh-flag-TR:empty": {
        "width": 16
    },
    "bfh-flag-TT:empty": {
        "width": 16
    },
    "bfh-flag-TV:empty": {
        "width": 16
    },
    "bfh-flag-TW:empty": {
        "width": 16
    },
    "bfh-flag-TZ:empty": {
        "width": 16
    },
    "bfh-flag-UA:empty": {
        "width": 16
    },
    "bfh-flag-UG:empty": {
        "width": 16
    },
    "bfh-flag-US:empty": {
        "width": 16
    },
    "bfh-flag-UY:empty": {
        "width": 16
    },
    "bfh-flag-UZ:empty": {
        "width": 16
    },
    "bfh-flag-VC:empty": {
        "width": 16
    },
    "bfh-flag-VE:empty": {
        "width": 16
    },
    "bfh-flag-VG:empty": {
        "width": 16
    },
    "bfh-flag-VI:empty": {
        "width": 16
    },
    "bfh-flag-VN:empty": {
        "width": 16
    },
    "bfh-flag-VU:empty": {
        "width": 16
    },
    "bfh-flag-WS:empty": {
        "width": 16
    },
    "bfh-flag-YE:empty": {
        "width": 16
    },
    "bfh-flag-ZA:empty": {
        "width": 16
    },
    "bfh-flag-ZM:empty": {
        "width": 16
    },
    "bfh-flag-BF:empty": {
        "width": 16
    },
    "bfh-flag-CU:empty": {
        "width": 16
    },
    "bfh-flag-DE:empty": {
        "width": 16
    },
    "bfh-flag-IR:empty": {
        "width": 16
    },
    "bfh-flag-KZ:empty": {
        "width": 16
    },
    "bfh-flag-LB:empty": {
        "width": 16
    },
    "bfh-flag-LI:empty": {
        "width": 16
    },
    "bfh-flag-MC:empty": {
        "width": 16
    },
    "bfh-flag-MD:empty": {
        "width": 16
    },
    "bfh-flag-MK:empty": {
        "width": 16
    },
    "bfh-flag-MN:empty": {
        "width": 16
    },
    "bfh-flag-MO:empty": {
        "width": 16
    },
    "bfh-flag-MX:empty": {
        "width": 16
    },
    "bfh-flag-MY:empty": {
        "width": 16
    },
    "bfh-flag-PR:empty": {
        "width": 16
    },
    "bfh-flag-RO:empty": {
        "width": 16
    },
    "bfh-flag-SL:empty": {
        "width": 16
    },
    "bfh-flag-TO:empty": {
        "width": 16
    },
    "bfh-flag-VA:empty": {
        "width": 16
    },
    "bfh-flag-ZW:empty": {
        "width": 16
    },
    "bfh-flag-EUR": {
        "marginRight": 5,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "width": 16,
        "height": 14,
        "backgroundPosition": "-96px -16px"
    },
    "bfh-flag-XCD": {
        "marginRight": 5,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "width": 16,
        "height": 14,
        "backgroundPosition": "-176px -80px"
    },
    "bfh-flag-AUD": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-32px 0"
    },
    "bfh-flag-CHF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-224px 0"
    },
    "bfh-flag-DKK": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-64px -16px"
    },
    "bfh-flag-XAF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-160px -80px"
    },
    "bfh-flag-XOF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-192px -80px"
    },
    "bfh-flag-XPF": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-208px -80px"
    },
    "bfh-flag-ZAR": {
        "width": 16,
        "height": 14,
        "background": "url(../img/bootstrap-formhelpers-currencies.flags.png) no-repeat",
        "marginRight": 5,
        "backgroundPosition": "-224px -80px"
    },
    "bfh-flag-AUD:empty": {
        "width": 16
    },
    "bfh-flag-CHF:empty": {
        "width": 16
    },
    "bfh-flag-DKK:empty": {
        "width": 16
    },
    "bfh-flag-EUR:empty": {
        "width": 16
    },
    "bfh-flag-XAF:empty": {
        "width": 16
    },
    "bfh-flag-XCD:empty": {
        "width": 16
    },
    "bfh-flag-XOF:empty": {
        "width": 16
    },
    "bfh-flag-XPF:empty": {
        "width": 16
    },
    "bfh-flag-ZAR:empty": {
        "width": 16
    },
    "bfh-selectbox": {
        "position": "relative"
    },
    "bfh-selectbox bfh-selectbox-toggle": {
        "display": "inline-block",
        "paddingTop": 6,
        "paddingRight": 24,
        "paddingBottom": 6,
        "paddingLeft": 12,
        "textDecoration": "none"
    },
    "bfh-selectbox bfh-selectbox-toggle:focus": {
        "outline": 0
    },
    "bfh-selectbox bfh-selectbox-toggle bfh-selectbox-option": {
        "display": "inline-block",
        "float": "left",
        "width": "100%",
        "height": 20,
        "overflow": "hidden",
        "textOverflow": "ellipsis"
    },
    "bfh-selectbox bfh-selectbox-toggle selectbox-caret": {
        "float": "right",
        "marginTop": 8,
        "marginRight": -16,
        "marginLeft": -10
    },
    "bfh-selectbox bfh-selectbox-options": {
        "position": "absolute",
        "top": "100%",
        "left": 0,
        "zIndex": 1000,
        "display": "none",
        "float": "left",
        "minWidth": 90,
        "paddingTop": 5,
        "paddingRight": 0,
        "paddingBottom": 5,
        "paddingLeft": 0,
        "marginTop": -1,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "fontSize": 14,
        "backgroundColor": "#fff",
        "border": "1px solid rgba(0,0,0,0.15)",
        "borderRadius": 4,
        "WebkitBoxShadow": "0 6px 12px rgba(0,0,0,0.175)",
        "boxShadow": "0 6px 12px rgba(0,0,0,0.175)",
        "backgroundClip": "padding-box"
    },
    "bfh-selectbox bfh-selectbox-optionspull-right": {
        "right": 0,
        "left": "auto"
    },
    "bfh-selectbox bfh-selectbox-options divider": {
        "height": 1,
        "marginTop": 9,
        "marginRight": 0,
        "marginBottom": 9,
        "marginLeft": 0,
        "overflow": "hidden",
        "backgroundColor": "#e5e5e5"
    },
    "bfh-selectbox bfh-selectbox-options bfh-selectbox-filter-container": {
        "width": "100%",
        "paddingTop": 5,
        "paddingRight": 5,
        "paddingBottom": 5,
        "paddingLeft": 5
    },
    "bfh-selectbox bfh-selectbox-options ul": {
        "maxWidth": 500,
        "maxHeight": 200,
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "marginTop": 5,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "overflowX": "hidden",
        "overflowY": "auto",
        "listStyle": "none"
    },
    "bfh-selectbox bfh-selectbox-options ul li>a": {
        "display": "block",
        "width": "100%",
        "minHeight": 26,
        "paddingTop": 3,
        "paddingRight": 20,
        "paddingBottom": 3,
        "paddingLeft": 20,
        "overflowX": "hidden",
        "clear": "both",
        "fontWeight": "normal",
        "lineHeight": 1.428571429,
        "color": "#333",
        "textOverflow": "ellipsis",
        "whiteSpace": "nowrap"
    },
    "bfh-selectbox bfh-selectbox-options ul li>a:hover": {
        "color": "#262626",
        "textDecoration": "none",
        "backgroundColor": "#f5f5f5"
    },
    "bfh-selectbox bfh-selectbox-options ul li>a:focus": {
        "color": "#262626",
        "textDecoration": "none",
        "backgroundColor": "#f5f5f5"
    },
    "bfh-selectbox bfh-selectbox-options ul bfh-selectbox-options-header": {
        "display": "block",
        "paddingTop": 3,
        "paddingRight": 20,
        "paddingBottom": 3,
        "paddingLeft": 20,
        "fontSize": 12,
        "lineHeight": 1.428571429,
        "color": "#999"
    },
    "bfh-selectbox bfh-selectbox-options ul disabled>a": {
        "color": "#999"
    },
    "bfh-selectbox bfh-selectbox-options ul disabled>a:hover": {
        "color": "#999",
        "textDecoration": "none",
        "cursor": "not-allowed",
        "backgroundColor": "transparent",
        "backgroundImage": "none",
        "filter": "progid:DXImageTransform.Microsoft.gradient(enabled=false)"
    },
    "bfh-selectbox bfh-selectbox-options ul disabled>a:focus": {
        "color": "#999",
        "textDecoration": "none",
        "cursor": "not-allowed",
        "backgroundColor": "transparent",
        "backgroundImage": "none",
        "filter": "progid:DXImageTransform.Microsoft.gradient(enabled=false)"
    },
    "bfh-selectboxopen>bfh-selectbox-options": {
        "display": "block"
    },
    "bfh-selectboxopen a": {
        "outline": 0
    },
    "pull-right>bfh-selectbox-options": {
        "right": 0,
        "left": "auto"
    },
    "bfh-selectbox-up caret": {
        "borderTop": "0 dotted",
        "borderBottom": "4px solid #000",
        "content": ""
    },
    "navbar-fixed-bottom bfh-selectbox caret": {
        "borderTop": "0 dotted",
        "borderBottom": "4px solid #000",
        "content": ""
    },
    "bfh-selectbox-up bfh-selectbox-options": {
        "top": "auto",
        "bottom": "100%",
        "marginBottom": 1
    },
    "navbar-fixed-bottom bfh-selectbox bfh-selectbox-options": {
        "top": "auto",
        "bottom": "100%",
        "marginBottom": 1
    },
    "bfh-googlefonts bfh-selectbox-options a": {
        "width": 230,
        "height": 30,
        "textIndent": -9999,
        "backgroundImage": "url(../img/bootstrap-formhelpers-googlefonts.png)"
    },
    "bfh-googlefonts bfh-selectbox-options a:focus": {
        "backgroundColor": "transparent",
        "backgroundRepeat": "no-repeat",
        "outline": 0,
        "filter": "none"
    },
    "bfh-googlefonts bfh-selectbox-options active>a": {
        "backgroundColor": "transparent",
        "backgroundImage": "url(../img/bootstrap-formhelpers-googlefonts.png)",
        "backgroundRepeat": "no-repeat",
        "outline": 0,
        "filter": "none"
    },
    "bfh-googlefonts bfh-selectbox-options active>a:hover": {
        "backgroundColor": "transparent",
        "backgroundImage": "url(../img/bootstrap-formhelpers-googlefonts.png)",
        "backgroundRepeat": "no-repeat",
        "outline": 0,
        "filter": "none"
    },
    "bfh-datepicker-calendar": {
        "position": "absolute",
        "top": "100%",
        "left": 0,
        "zIndex": 1000,
        "display": "none",
        "float": "left",
        "minWidth": 296
    },
    "bfh-datepicker-calendar>tablecalendar": {
        "width": 376,
        "background": "#fff"
    },
    "bfh-datepicker-calendar>tablecalendar months-header>th": {
        "fontSize": 12,
        "textAlign": "center"
    },
    "bfh-datepicker-calendar>tablecalendar months-header>thmonth>span": {
        "display": "inline-block",
        "width": 100
    },
    "bfh-datepicker-calendar>tablecalendar months-header>thyear>span": {
        "display": "inline-block",
        "width": 50
    },
    "bfh-datepicker-calendar>tablecalendar days-header>th": {
        "width": 30,
        "fontSize": 11,
        "lineHeight": 12,
        "textAlign": "center"
    },
    "bfh-datepicker-calendar>tablecalendar>tbody>tr>td": {
        "width": 30,
        "fontSize": 11,
        "lineHeight": 12,
        "textAlign": "center"
    },
    "bfh-datepicker-calendar>tablecalendar>tbody>tr>tdtoday": {
        "color": "#fff",
        "backgroundColor": "#428bca"
    },
    "bfh-datepicker-calendar>tablecalendar>tbody>tr>tdoff": {
        "color": "#999"
    },
    "bfh-datepicker-calendar>tablecalendar>tbody>tr>td:not(off):hover": {
        "color": "#262626",
        "cursor": "pointer",
        "backgroundColor": "#f5f5f5"
    },
    "bfh-datepicker": {
        "position": "relative"
    },
    "bfh-datepicker-toggle": {
        "MarginBottom": -3
    },
    "bfh-datepicker-toggle>input[readonly]": {
        "cursor": "inherit",
        "backgroundColor": "inherit"
    },
    "open>bfh-datepicker-calendar": {
        "display": "block"
    },
    "bfh-timepicker-popover": {
        "position": "absolute",
        "top": "100%",
        "left": 0,
        "zIndex": 1000,
        "display": "none",
        "float": "left",
        "minWidth": 100,
        "backgroundColor": "#fff",
        "border": "1px solid rgba(0,0,0,0.15)",
        "borderRadius": 4,
        "WebkitBoxShadow": "0 6px 12px rgba(0,0,0,0.175)",
        "boxShadow": "0 6px 12px rgba(0,0,0,0.175)",
        "backgroundClip": "padding-box"
    },
    "bfh-timepicker-popover>table": {
        "width": 180,
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0
    },
    "bfh-timepicker-popover>table>tbody>tr>td": {
        "textAlign": "center",
        "border": 0
    },
    "bfh-timepicker-popover>table>tbody>tr>tdseparator": {
        "fontSize": 20,
        "fontWeight": "bold",
        "lineHeight": 28
    },
    "bfh-timepicker-popover>table>tbody>tr>td>div>input": {
        "width": "42px!important",
        "textAlign": "center"
    },
    "bfh-timepicker": {
        "position": "relative"
    },
    "bfh-timepicker-toggle": {
        "MarginBottom": -3
    },
    "bfh-timepicker-toggle>input[readonly]": {
        "cursor": "inherit",
        "backgroundColor": "inherit"
    },
    "open>bfh-timepicker-popover": {
        "display": "block"
    },
    "bfh-slider": {
        "height": 20,
        "marginTop": 8,
        "marginBottom": 23,
        "backgroundColor": "#fff",
        "border": "1px solid rgba(0,0,0,0.15)",
        "borderRadius": 4,
        "WebkitUserSelect": "none",
        "KhtmlUserSelect": "none",
        "MozUserSelect": "none",
        "OUserSelect": "none"
    },
    "bfh-slider>bfh-slider-handle": {
        "position": "absolute",
        "width": 20,
        "height": 34,
        "marginTop": -7,
        "cursor": "col-resize",
        "background": "#efefef",
        "border": "1px solid rgba(0,0,0,0.15)",
        "borderRadius": 4
    },
    "bfh-slider>bfh-slider-handle>bfh-slider-value": {
        "position": "absolute",
        "width": 48,
        "height": 20,
        "marginTop": 5,
        "marginLeft": -15,
        "lineHeight": 20,
        "textAlign": "center",
        "cursor": "col-resize",
        "backgroundColor": "#fff",
        "border": "1px solid rgba(0,0,0,0.15)",
        "borderRadius": 4
    },
    "bfh-slider>bfh-slider-handle>bfh-slider-value disabled": {
        "color": "#999"
    },
    "bfh-sliderdisabled bfh-slider-value": {
        "color": "#999"
    },
    "bfh-colorpicker-popover": {
        "position": "absolute",
        "top": "100%",
        "left": 0,
        "zIndex": 1000,
        "display": "none",
        "float": "left",
        "minWidth": 100,
        "paddingTop": 20,
        "paddingRight": 20,
        "paddingBottom": 20,
        "paddingLeft": 20,
        "cursor": "default",
        "backgroundColor": "#fff",
        "border": "1px solid rgba(0,0,0,0.15)",
        "borderRadius": 4,
        "WebkitBoxShadow": "0 6px 12px rgba(0,0,0,0.175)",
        "boxShadow": "0 6px 12px rgba(0,0,0,0.175)",
        "backgroundClip": "padding-box"
    },
    "bfh-colorpicker-popover>canvas": {
        "width": 384,
        "height": 256
    },
    "bfh-colorpicker": {
        "position": "relative"
    },
    "bfh-colorpicker-toggle": {
        "MarginBottom": -3
    },
    "bfh-colorpicker-toggle>input[readonly]": {
        "cursor": "inherit",
        "backgroundColor": "inherit"
    },
    "bfh-colorpicker-toggle bfh-colorpicker-icon": {
        "display": "block",
        "width": 16,
        "height": 16
    },
    "open>bfh-colorpicker-popover": {
        "display": "block"
    },
    "input-group>bfh-number": {
        "borderRight": 0
    },
    "input-group>bfh-number-btn:hover": {
        "backgroundColor": "#ccc"
    }
});