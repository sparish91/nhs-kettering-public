import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "body": {
        "position": "relative",
        "fontFamily": "'avantgarde_ceregular'",
        "letterSpacing": 1
    },
    "p": {
        "fontFamily": "'lato'",
        "textAlign": "center",
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "fontSize": 12,
        "color": "#fff"
    },
    "li": {
        "fontFamily": "'lato'",
        "color": "#002b00",
        "fontSize": 18,
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "textAlign": "left"
    },
    "h1": {
        "color": "#fff"
    },
    "h2": {
        "color": "#002b00",
        "fontSize": 30
    },
    "h3": {
        "color": "#fff"
    },
    "h4": {
        "color": "#fff"
    },
    "h5": {
        "color": "#fff"
    },
    "h6": {
        "color": "#fff"
    },
    "label": {
        "color": "#fff"
    },
    "header": {
        "position": "relative",
        "textAlign": "center",
        "backgroundColor": "#fff"
    },
    "header img": {
        "marginTop": 65,
        "marginRight": 0,
        "marginBottom": 65,
        "marginLeft": 0
    },
    "a": {
        "color": "#185d6c"
    },
    "a:hover": {
        "color": "#185d6c",
        "textDecoration": "none"
    },
    "free_sub_form": {
        "marginTop": 1
    },
    "navbar": {
        "border": "1px solid transparent",
        "marginBottom": 0,
        "minHeight": 0,
        "position": "relative"
    },
    "navbar-default": {
        "backgroundColor": "transparent",
        "borderColor": "transparent"
    },
    "navbar-default navbar-nav > li > a": {
        "color": "#000",
        "fontSize": 12
    },
    "navbar-toggle": {
        "zIndex": 10000000,
        "marginTop": 14,
        "marginRight": 0,
        "background": "#000",
        "borderRadius": 15,
        "position": "absolute",
        "right": 15
    },
    "navbar-toggle icon-bar": {
        "background": "#fff none repeat scroll 0 0"
    },
    "navbar-default navbar-collapse": {
        "borderColor": "transparent"
    },
    "navbar-default navbar-form": {
        "borderColor": "transparent"
    },
    "navbar-default navbar-nav > li > a:hover": {
        "backgroundColor": "transparent",
        "color": "#000",
        "textDecoration": "underline"
    },
    "navbar-default navbar-nav > li > a:focus": {
        "backgroundColor": "transparent",
        "color": "#000",
        "textDecoration": "underline"
    },
    "mobile-slider": {
        "height": 250,
        "zIndex": -1
    },
    "spark-box-start accordion-toggle": {
        "display": "none"
    },
    "spark-box-start collapse": {
        "display": "block"
    },
    "panel-default > panel-heading": {
        "color": "#FFF",
        "backgroundColor": "transparent",
        "border": "none",
        "paddingTop": 0
    },
    "panel-default > panel-heading a": {
        "color": "#FFF"
    },
    "panel-default > panel-body form": {
        "marginTop": 2
    },
    "panel-default > panel-body p": {
        "fontSize": 14
    },
    "panel-default > panel-body p a": {
        "textDecoration": "underline",
        "color": "#fff"
    },
    "breadcrumb": {
        "backgroundColor": "transparent",
        "borderRadius": 0,
        "listStyle": "outside none none",
        "marginBottom": 0,
        "paddingTop": 0,
        "paddingRight": 15,
        "paddingBottom": 8,
        "paddingLeft": 15,
        "borderBottom": "1px solid #fff"
    },
    "panel": {
        "background": "transparent",
        "borderColor": "transparent",
        "border": "0 none"
    },
    "list-group-item": {
        "marginBottom": 1
    },
    "badge": {
        "display": "none"
    },
    "panel-group": {
        "marginBottom": 0
    },
    "panel-title>a": {
        "color": "#FFF"
    },
    "spark-box-start": {
        "marginTop": "auto",
        "boxShadow": "none",
        "width": "90%",
        "marginRight": "auto",
        "marginBottom": "auto",
        "marginLeft": "auto"
    },
    "panel-body": {
        "paddingTop": 4,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "panel-body p": {
        "marginBottom": 15
    },
    "has-feedback form-control-feedback": {
        "top": 31
    },
    "strap": {
        "textAlign": "right",
        "color": "#FFF",
        "marginTop": 1,
        "fontSize": 20,
        "fontWeight": "bold"
    },
    "spark_inner": {
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "afree_sub": {
        "color": "#fff",
        "textAlign": "center",
        "paddingTop": 1.2,
        "paddingRight": 1.2,
        "paddingBottom": 1.2,
        "paddingLeft": 1.2,
        "border": "0 none",
        "background": "#208e60",
        "fontSize": 18
    },
    "buttonproceed": {
        "color": "#fff",
        "textAlign": "center",
        "paddingTop": 16,
        "paddingRight": 16,
        "paddingBottom": 16,
        "paddingLeft": 16,
        "border": "1px solid #fff",
        "background": "#007042",
        "fontSize": 20,
        "textDecoration": "none!important",
        "display": "block",
        "marginTop": 16,
        "marginRight": "auto",
        "marginBottom": 16,
        "marginLeft": "auto",
        "fontWeight": "bold",
        "borderRadius": 0,
        "width": "90%",
        "fontFamily": "'lato'",
        "lineHeight": 1
    },
    "afree_sub:hover": {
        "background": "#007042",
        "textDecoration": "none!important",
        "display": "block",
        "marginTop": 16,
        "marginRight": "auto",
        "marginBottom": 16,
        "marginLeft": "auto",
        "fontSize": 20,
        "paddingTop": 16,
        "paddingRight": 16,
        "paddingBottom": 16,
        "paddingLeft": 16,
        "fontWeight": "bold",
        "borderRadius": 0,
        "border": "1px solid #fff",
        "width": "90%",
        "fontFamily": "'lato'",
        "lineHeight": 1
    },
    "buttonproceed:hover": {
        "background": "#007042",
        "textDecoration": "none!important",
        "display": "block",
        "marginTop": 16,
        "marginRight": "auto",
        "marginBottom": 16,
        "marginLeft": "auto",
        "fontSize": 20,
        "paddingTop": 16,
        "paddingRight": 16,
        "paddingBottom": 16,
        "paddingLeft": 16,
        "fontWeight": "bold",
        "borderRadius": 0,
        "border": "1px solid #fff",
        "width": "90%",
        "fontFamily": "'lato'",
        "lineHeight": 1
    },
    "register": {
        "background": "#007042",
        "textDecoration": "none!important",
        "display": "block",
        "marginTop": 16,
        "marginRight": "auto",
        "marginBottom": 16,
        "marginLeft": "auto",
        "fontSize": 20,
        "paddingTop": 16,
        "paddingRight": 16,
        "paddingBottom": 16,
        "paddingLeft": 16,
        "fontWeight": "bold",
        "borderRadius": 0,
        "border": "1px solid #fff",
        "width": "90%",
        "fontFamily": "'lato'",
        "lineHeight": 1
    },
    "register:hover": {
        "background": "#007042",
        "textDecoration": "none!important",
        "display": "block",
        "marginTop": 16,
        "marginRight": "auto",
        "marginBottom": 16,
        "marginLeft": "auto",
        "fontSize": 20,
        "paddingTop": 16,
        "paddingRight": 16,
        "paddingBottom": 16,
        "paddingLeft": 16,
        "fontWeight": "bold",
        "borderRadius": 0,
        "border": "1px solid #fff",
        "width": "90%",
        "fontFamily": "'lato'",
        "lineHeight": 1
    },
    "ahome": {
        "display": "none"
    },
    "breadcrumb > li": {
        "display": "block",
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto"
    },
    "breadcrumb h1": {
        "textAlign": "center",
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "fontSize": 32,
        "color": "#fff",
        "textTransform": "uppercase"
    },
    "breadcrumb h1 span": {
        "display": "block",
        "color": "#045b36",
        "letterSpacing": 6
    },
    "breadcrumb > li + li::before": {
        "color": "#ccc",
        "content": "",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "form-control": {
        "height": 45,
        "MozBoxShadow": "1px 2px 5px #888 inset",
        "WebkitBoxShadow": "1px 2px 5px #888 inset",
        "boxShadow": "1px 2px 5px #888 inset",
        "borderRadius": 0,
        "fontSize": 18
    },
    "aspark-logo": {
        "position": "relative",
        "display": "block",
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 1,
        "paddingRight": 1,
        "paddingBottom": 1,
        "paddingLeft": 1
    },
    "aspark-logo img": {
        "display": "block",
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto"
    },
    "afujitsu-logo": {
        "position": "relative",
        "display": "block",
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto",
        "paddingTop": 1,
        "paddingRight": 1,
        "paddingBottom": 1,
        "paddingLeft": 1,
        "textAlign": "center",
        "color": "#000"
    },
    "afujitsu-logo img": {
        "display": "block",
        "marginTop": 0,
        "marginRight": "auto",
        "marginBottom": 0,
        "marginLeft": "auto"
    },
    "big-modal": {
        "background": "#FFF",
        "height": "100%",
        "left": 0,
        "position": "fixed",
        "right": 0,
        "top": 0,
        "width": "100%",
        "zIndex": 999999999,
        "display": "none",
        "overflow": "auto"
    },
    "big-modal h1": {
        "marginTop": 2,
        "color": "#002b00",
        "fontSize": 30
    },
    "big-modal h3": {
        "fontSize": 23,
        "color": "#002b00"
    },
    "big-modal p": {
        "color": "#002b00",
        "fontSize": 18,
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "textAlign": "left"
    },
    "close-button": {
        "position": "absolute",
        "right": 0,
        "top": 0,
        "marginRight": 1,
        "marginTop": 1
    },
    "content": {
        "maxWidth": 450,
        "backgroundColor": "#2d2926",
        "paddingTop": 15,
        "paddingRight": 15,
        "paddingBottom": 0,
        "paddingLeft": 15,
        "borderTop": "4px solid #045b36",
        "borderBottom": "4px solid #045b36",
        "borderRadius": 8
    },
    "spark_form": {
        "maxWidth": 450,
        "backgroundColor": "#2d2926",
        "paddingTop": 15,
        "paddingRight": 15,
        "paddingBottom": 0,
        "paddingLeft": 15,
        "borderTop": "4px solid #045b36",
        "borderBottom": "4px solid #045b36",
        "borderRadius": 8
    },
    "content a": {
        "color": "#fff",
        "textDecoration": "underline"
    },
    "content a:hover": {
        "color": "#fff",
        "textDecoration": "underline"
    },
    "content heading h4": {
        "fontFamily": "'lato'"
    },
    "wifi_support": {
        "position": "fixed",
        "bottom": 0,
        "right": 0,
        "left": 0,
        "paddingTop": 8,
        "paddingRight": 8,
        "paddingBottom": 8,
        "paddingLeft": 8,
        "background": "#f7f7f7",
        "overflowY": "scroll"
    },
    "wifi_support::-webkit-scrollbar": {
        "display": "none"
    },
    "wifi_support:hover": {
        "cursor": "pointer"
    },
    "wifi_support > p": {
        "textAlign": "center!important"
    },
    "wifi_support p": {
        "color": "#2d2926",
        "fontSize": 16,
        "textAlign": "left"
    },
    "wifi_support li": {
        "color": "#2d2926",
        "fontSize": 16,
        "textAlign": "left"
    },
    "wifi_support a": {
        "textDecoration": "underline",
        "color": "#626262"
    },
    "wifi_support i": {
        "fontSize": 20,
        "transform": "rotate(180deg)"
    },
    "wifi_support body": {
        "display": "none"
    },
    "wifi_support bodypopulated": {
        "paddingTop": 15,
        "paddingRight": 15,
        "paddingBottom": 15,
        "paddingLeft": 15,
        "display": "block"
    },
    "language": {
        "maxWidth": 400,
        "textAlign": "right"
    },
    "language bfh-selectbox-toggle": {
        "boxShadow": "none",
        "border": "none"
    },
    "languagepicker": {
        "position": "relative",
        "marginRight": 15
    },
    "languagepicker li:not([class*=\"selected\"])": {
        "display": "none"
    },
    "languagepicker liselected": {
        "position": "relative"
    },
    "languagepicker liselected:before": {
        "content": "\\f103",
        "font": "normal normal normal 20px/1 FontAwesome",
        "position": "absolute",
        "right": -20,
        "top": "25%"
    },
    "languagepicker liselected:hover": {
        "cursor": "pointer"
    },
    "languagepicker li:not(selected)": {
        "listStyle": "none",
        "position": "absolute",
        "right": 0,
        "background": "#fff",
        "borderRadius": 4
    },
    "languagepicker li:not(selected) div:hover": {
        "cursor": "pointer"
    },
    "friendlywifi": {
        "marginTop": 2
    },
    "termsModal": {
        "overflowY": "scroll"
    },
    "termsModal::-webkit-scrollbar": {
        "display": "none"
    },
    "modal-dialog": {
        "width": "90%",
        "marginTop": "5%",
        "marginRight": "auto",
        "marginBottom": "5%",
        "marginLeft": "auto"
    },
    "modal-content": {
        "backgroundColor": "#f7f7f7",
        "color": "#626262"
    },
    "modal-content p": {
        "color": "#626262"
    },
    "modal-content h4": {
        "color": "#626262"
    },
    "modal-content a": {
        "color": "#626262",
        "textDecoration": "underline"
    },
    "btn-success": {
        "backgroundColor": "#045b36",
        "borderColor": "#045b36"
    },
    "btn-success:hover": {
        "backgroundColor": "#045b36",
        "borderColor": "#045b36"
    },
    "::-moz-selection": {
        "background": "#b3d4fc",
        "textShadow": "none"
    },
    "::selection": {
        "background": "#b3d4fc",
        "textShadow": "none"
    },
    "hr": {
        "display": "block",
        "height": 1,
        "border": 0,
        "borderTop": "1px solid #ccc",
        "marginTop": 1,
        "marginRight": 0,
        "marginBottom": 1,
        "marginLeft": 0,
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "audio": {
        "verticalAlign": "middle"
    },
    "canvas": {
        "verticalAlign": "middle"
    },
    "iframe": {
        "verticalAlign": "middle"
    },
    "img": {
        "verticalAlign": "middle"
    },
    "svg": {
        "verticalAlign": "middle"
    },
    "video": {
        "verticalAlign": "middle"
    },
    "fieldset": {
        "border": 0,
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0
    },
    "textarea": {
        "resize": "vertical"
    },
    "browserupgrade": {
        "marginTop": 0.2,
        "marginRight": 0,
        "marginBottom": 0.2,
        "marginLeft": 0,
        "background": "#ccc",
        "color": "#000",
        "paddingTop": 0.2,
        "paddingRight": 0,
        "paddingBottom": 0.2,
        "paddingLeft": 0
    },
    "hidden": {
        "display": "none !important"
    },
    "visuallyhidden": {
        "border": 0,
        "clip": "rect(0 0 0 0)",
        "height": 1,
        "marginTop": -1,
        "marginRight": -1,
        "marginBottom": -1,
        "marginLeft": -1,
        "overflow": "hidden",
        "paddingTop": 0,
        "paddingRight": 0,
        "paddingBottom": 0,
        "paddingLeft": 0,
        "position": "absolute",
        "width": 1
    },
    "visuallyhiddenfocusable:active": {
        "clip": "auto",
        "height": "auto",
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "overflow": "visible",
        "position": "static",
        "width": "auto"
    },
    "visuallyhiddenfocusable:focus": {
        "clip": "auto",
        "height": "auto",
        "marginTop": 0,
        "marginRight": 0,
        "marginBottom": 0,
        "marginLeft": 0,
        "overflow": "visible",
        "position": "static",
        "width": "auto"
    },
    "invisible": {
        "visibility": "hidden"
    },
    "clearfix:before": {
        "content": " ",
        "display": "table"
    },
    "clearfix:after": {
        "content": " ",
        "display": "table",
        "clear": "both"
    }
});