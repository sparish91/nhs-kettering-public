import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "flag": {
        "display": "inline-block",
        "width": 32,
        "height": 32,
        "background": "url('flags.png') no-repeat"
    },
    "flagflag-bih": {
        "backgroundPosition": "0 -32px"
    },
    "flagflag-cod": {
        "backgroundPosition": "-128px -64px"
    },
    "flagflag-lca": {
        "backgroundPosition": "-416px -224px"
    },
    "flagflag-tjk": {
        "backgroundPosition": "-96px -416px"
    },
    "flagflag-aia": {
        "backgroundPosition": "-128px 0"
    },
    "flagflag-plw": {
        "backgroundPosition": "-128px -352px"
    },
    "flagflag-guy": {
        "backgroundPosition": "-288px -160px"
    },
    "flagflag-gha": {
        "backgroundPosition": "-416px -128px"
    },
    "flagflag-ner": {
        "backgroundPosition": "0 -320px"
    },
    "flagflag-swz": {
        "backgroundPosition": "-416px -384px"
    },
    "flagflag-tto": {
        "backgroundPosition": "-288px -416px"
    },
    "flagflag-nor": {
        "backgroundPosition": "-128px -320px"
    },
    "flagflag-tuv": {
        "backgroundPosition": "-320px -416px"
    },
    "flagflag-brb": {
        "backgroundPosition": "-32px -32px"
    },
    "flagflag-svk": {
        "backgroundPosition": "-128px -384px"
    },
    "flagflag-geo": {
        "backgroundPosition": "-320px -128px"
    },
    "flagflag-slv": {
        "backgroundPosition": "-352px -384px"
    },
    "flagflag-glp": {
        "backgroundPosition": "-96px -160px"
    },
    "flagflag-aze": {
        "backgroundPosition": "-448px 0"
    },
    "flagflag-asm": {
        "backgroundPosition": "-320px 0"
    },
    "flagflag-vat": {
        "backgroundPosition": "-96px -448px"
    },
    "flagflag-irl": {
        "backgroundPosition": "-32px -192px"
    },
    "flagflag-lao": {
        "backgroundPosition": "-352px -224px"
    },
    "flagflag-lka": {
        "backgroundPosition": "0 -256px"
    },
    "flagflag-ury": {
        "backgroundPosition": "-32px -448px"
    },
    "flagflag-svn": {
        "backgroundPosition": "-96px -384px"
    },
    "flagflag-dma": {
        "backgroundPosition": "-224px -96px"
    },
    "flagflag-pol": {
        "backgroundPosition": "0 -352px"
    },
    "flagflag-irq": {
        "backgroundPosition": "-192px -192px"
    },
    "flagflag-blz": {
        "backgroundPosition": "-64px -64px"
    },
    "flagflag-abw": {
        "backgroundPosition": "-416px 0"
    },
    "flagflag-sgp": {
        "backgroundPosition": "-64px -384px"
    },
    "flagflag-slb": {
        "backgroundPosition": "-416px -352px"
    },
    "flagflag-dza": {
        "backgroundPosition": "-288px -96px"
    },
    "flagflag-kaz": {
        "backgroundPosition": "-320px -224px"
    },
    "flagflag-nga": {
        "backgroundPosition": "-32px -320px"
    },
    "flagflag-hti": {
        "backgroundPosition": "-416px -160px"
    },
    "flagflag-fsm": {
        "backgroundPosition": "-128px -128px"
    },
    "flagflag-idn": {
        "backgroundPosition": "0 -192px"
    },
    "flagflag-twn": {
        "backgroundPosition": "-352px -416px"
    },
    "flagflag-grd": {
        "backgroundPosition": "-288px -128px"
    },
    "flagflag-mar": {
        "backgroundPosition": "-224px -256px"
    },
    "flagflag-nam": {
        "backgroundPosition": "-416px -288px"
    },
    "flagflag-qat": {
        "backgroundPosition": "-192px -352px"
    },
    "flagflag-and": {
        "backgroundPosition": "0 0"
    },
    "flagflag-mkd": {
        "backgroundPosition": "-416px -256px"
    },
    "flagflag-chl": {
        "backgroundPosition": "-320px -64px"
    },
    "flagflag-kwt": {
        "backgroundPosition": "-256px -224px"
    },
    "flagflag-smr": {
        "backgroundPosition": "-192px -384px"
    },
    "flagflag-can": {
        "backgroundPosition": "-96px -64px"
    },
    "flagflag-are": {
        "backgroundPosition": "-32px 0"
    },
    "flagflag-vgb": {
        "backgroundPosition": "-192px -448px"
    },
    "flagflag-sle": {
        "backgroundPosition": "-160px -384px"
    },
    "flagflag-mlt": {
        "backgroundPosition": "-192px -288px"
    },
    "flagflag-ven": {
        "backgroundPosition": "-160px -448px"
    },
    "flagflag-tls": {
        "backgroundPosition": "-128px -416px"
    },
    "flagflag-atg": {
        "backgroundPosition": "-96px 0"
    },
    "flagflag-tcd": {
        "backgroundPosition": "0 -416px"
    },
    "flagflag-moz": {
        "backgroundPosition": "-384px -288px"
    },
    "flagflag-rou": {
        "backgroundPosition": "-256px -352px"
    },
    "flagflag-gtm": {
        "backgroundPosition": "-192px -160px"
    },
    "flagflag-phl": {
        "backgroundPosition": "-416px -320px"
    },
    "flagflag-ggy": {
        "backgroundPosition": "-384px -128px"
    },
    "flagflag-ncl": {
        "backgroundPosition": "-448px -288px"
    },
    "flagflag-prt": {
        "backgroundPosition": "-96px -352px"
    },
    "flagflag-arg": {
        "backgroundPosition": "-288px 0"
    },
    "flagflag-bwa": {
        "backgroundPosition": "0 -64px"
    },
    "flagflag-bfa": {
        "backgroundPosition": "-128px -32px"
    },
    "flagflag-bol": {
        "backgroundPosition": "-352px -32px"
    },
    "flagflag-aus": {
        "backgroundPosition": "-384px 0"
    },
    "flagflag-grl": {
        "backgroundPosition": "0 -160px"
    },
    "flagflag-dnk": {
        "backgroundPosition": "-192px -96px"
    },
    "flagflag-fro": {
        "backgroundPosition": "-160px -128px"
    },
    "flagflag-usa": {
        "backgroundPosition": "0 -448px"
    },
    "flagflag-nld": {
        "backgroundPosition": "-96px -320px"
    },
    "flagflag-gab": {
        "backgroundPosition": "-224px -128px"
    },
    "flagflag-omn": {
        "backgroundPosition": "-256px -320px"
    },
    "flagflag-ton": {
        "backgroundPosition": "-224px -416px"
    },
    "flagflag-jpn": {
        "backgroundPosition": "-448px -192px"
    },
    "flagflag-nzl": {
        "backgroundPosition": "-224px -320px"
    },
    "flagflag-aut": {
        "backgroundPosition": "-352px 0"
    },
    "flagflag-npl": {
        "backgroundPosition": "-160px -320px"
    },
    "flagflag-lby": {
        "backgroundPosition": "-192px -256px"
    },
    "flagflag-isl": {
        "backgroundPosition": "-256px -192px"
    },
    "flagflag-mda": {
        "backgroundPosition": "-288px -256px"
    },
    "flagflag-tha": {
        "backgroundPosition": "-64px -416px"
    },
    "flagflag-cym": {
        "backgroundPosition": "-288px -224px"
    },
    "flagflag-kgz": {
        "backgroundPosition": "-32px -224px"
    },
    "flagflag-bdi": {
        "backgroundPosition": "-224px -32px"
    },
    "flagflag-lva": {
        "backgroundPosition": "-160px -256px"
    },
    "flagflag-fji": {
        "backgroundPosition": "-96px -128px"
    },
    "flagflag-afg": {
        "backgroundPosition": "-64px 0"
    },
    "flagflag-cze": {
        "backgroundPosition": "-96px -96px"
    },
    "flagflag-mne": {
        "backgroundPosition": "-320px -256px"
    },
    "flagflag-esh": {
        "backgroundPosition": "-416px -96px"
    },
    "flagflag-ben": {
        "backgroundPosition": "-256px -32px"
    },
    "flagflag-mrt": {
        "backgroundPosition": "-128px -288px"
    },
    "flagflag-an": {
        "backgroundPosition": "-224px 0"
    },
    "flagflag-deu": {
        "backgroundPosition": "-128px -96px"
    },
    "flagflag-mhl": {
        "backgroundPosition": "-384px -256px"
    },
    "flagflag-imn": {
        "backgroundPosition": "-128px -192px"
    },
    "flagflag-tun": {
        "backgroundPosition": "-192px -416px"
    },
    "flagflag-lso": {
        "backgroundPosition": "-64px -256px"
    },
    "flagflag-eri": {
        "backgroundPosition": "-448px -96px"
    },
    "flagflag-uga": {
        "backgroundPosition": "-448px -416px"
    },
    "flagflag-png": {
        "backgroundPosition": "-384px -320px"
    },
    "flagflag-tza": {
        "backgroundPosition": "-384px -416px"
    },
    "flagflag-mwi": {
        "backgroundPosition": "-288px -288px"
    },
    "flagflag-vir": {
        "backgroundPosition": "-224px -448px"
    },
    "flagflag-pyf": {
        "backgroundPosition": "-352px -320px"
    },
    "flagflag-jey": {
        "backgroundPosition": "-352px -192px"
    },
    "flagflag-fin": {
        "backgroundPosition": "-64px -128px"
    },
    "flagflag-zwe": {
        "backgroundPosition": "-448px -448px"
    },
    "flagflag-mac": {
        "backgroundPosition": "-64px -288px"
    },
    "flagflag-est": {
        "backgroundPosition": "-352px -96px"
    },
    "flagflag-gum": {
        "backgroundPosition": "-224px -160px"
    },
    "flagflag-bra": {
        "backgroundPosition": "-384px -32px"
    },
    "flagflag-hnd": {
        "backgroundPosition": "-352px -160px"
    },
    "flagflag-cmr": {
        "backgroundPosition": "-352px -64px"
    },
    "flagflag-btn": {
        "backgroundPosition": "-448px -32px"
    },
    "flagflag-ecu": {
        "backgroundPosition": "-320px -96px"
    },
    "flagflag-ago": {
        "backgroundPosition": "-256px 0"
    },
    "flagflag-ukr": {
        "backgroundPosition": "-416px -416px"
    },
    "flagflag-com": {
        "backgroundPosition": "-128px -224px"
    },
    "flagflag-syr": {
        "backgroundPosition": "-384px -384px"
    },
    "flagflag-lbr": {
        "backgroundPosition": "-32px -256px"
    },
    "flagflag-mdg": {
        "backgroundPosition": "-352px -256px"
    },
    "flagflag-jor": {
        "backgroundPosition": "-416px -192px"
    },
    "flagflag-bgd": {
        "backgroundPosition": "-64px -32px"
    },
    "flagflag-tgo": {
        "backgroundPosition": "-32px -416px"
    },
    "flagflag-tkm": {
        "backgroundPosition": "-160px -416px"
    },
    "flagflag-bhr": {
        "backgroundPosition": "-192px -32px"
    },
    "flagflag-vnm": {
        "backgroundPosition": "-256px -448px"
    },
    "flagflag-ken": {
        "backgroundPosition": "0 -224px"
    },
    "flagflag-chn": {
        "backgroundPosition": "-384px -64px"
    },
    "flagflag-pse": {
        "backgroundPosition": "-64px -352px"
    },
    "flagflag-hrv": {
        "backgroundPosition": "-384px -160px"
    },
    "flagflag-mco": {
        "backgroundPosition": "-256px -256px"
    },
    "flagflag-kna": {
        "backgroundPosition": "-160px -224px"
    },
    "flagflag-rwa": {
        "backgroundPosition": "-352px -352px"
    },
    "flagflag-mli": {
        "backgroundPosition": "-448px -256px"
    },
    "flagflag-gmb": {
        "backgroundPosition": "-32px -160px"
    },
    "flagflag-pak": {
        "backgroundPosition": "-448px -320px"
    },
    "flagflag-mdv": {
        "backgroundPosition": "-256px -288px"
    },
    "flagflag-grc": {
        "backgroundPosition": "-160px -160px"
    },
    "flagflag-ind": {
        "backgroundPosition": "-160px -192px"
    },
    "flagflag-gnb": {
        "backgroundPosition": "-256px -160px"
    },
    "flagflag-cok": {
        "backgroundPosition": "-288px -64px"
    },
    "flagflag-fra": {
        "backgroundPosition": "-192px -128px"
    },
    "flagflag-sau": {
        "backgroundPosition": "-384px -352px"
    },
    "flagflag-zaf": {
        "backgroundPosition": "-384px -448px"
    },
    "flagflag-mex": {
        "backgroundPosition": "-320px -288px"
    },
    "flagflag-ita": {
        "backgroundPosition": "-288px -192px"
    },
    "flagflag-col": {
        "backgroundPosition": "-416px -64px"
    },
    "flagflag-tca": {
        "backgroundPosition": "-448px -384px"
    },
    "flagflag-swe": {
        "backgroundPosition": "-32px -384px"
    },
    "flagflag-mng": {
        "backgroundPosition": "-32px -288px"
    },
    "flagflag-msr": {
        "backgroundPosition": "-160px -288px"
    },
    "flagflag-gib": {
        "backgroundPosition": "-448px -128px"
    },
    "flagflag-alb": {
        "backgroundPosition": "-160px 0"
    },
    "flagflag-pry": {
        "backgroundPosition": "-160px -352px"
    },
    "flagflag-civ": {
        "backgroundPosition": "-256px -64px"
    },
    "flagflag-caf": {
        "backgroundPosition": "-160px -64px"
    },
    "flagflag-yem": {
        "backgroundPosition": "-352px -448px"
    },
    "flagflag-irn": {
        "backgroundPosition": "-224px -192px"
    },
    "flagflag-zmb": {
        "backgroundPosition": "-416px -448px"
    },
    "flagflag-vut": {
        "backgroundPosition": "-288px -448px"
    },
    "flagflag-ltu": {
        "backgroundPosition": "-96px -256px"
    },
    "flagflag-pan": {
        "backgroundPosition": "-288px -320px"
    },
    "flagflag-isr": {
        "backgroundPosition": "-64px -192px"
    },
    "flagflag-kir": {
        "backgroundPosition": "-96px -224px"
    },
    "flagflag-rus": {
        "backgroundPosition": "-320px -352px"
    },
    "flagflag-eth": {
        "backgroundPosition": "-32px -128px"
    },
    "flagflag-mmr": {
        "backgroundPosition": "0 -288px"
    },
    "flagflag-egy": {
        "backgroundPosition": "-384px -96px"
    },
    "flagflag-prk": {
        "backgroundPosition": "-192px -224px"
    },
    "flagflag-gnq": {
        "backgroundPosition": "-128px -160px"
    },
    "flagflag-lie": {
        "backgroundPosition": "-448px -224px"
    },
    "flagflag-nru": {
        "backgroundPosition": "-192px -320px"
    },
    "flagflag-cub": {
        "backgroundPosition": "0 -96px"
    },
    "flagflag-bhs": {
        "backgroundPosition": "-416px -32px"
    },
    "flagflag-kor": {
        "backgroundPosition": "-224px -224px"
    },
    "flagflag-hun": {
        "backgroundPosition": "-448px -160px"
    },
    "flagflag-arm": {
        "backgroundPosition": "-192px 0"
    },
    "flagflag-tur": {
        "backgroundPosition": "-256px -416px"
    },
    "flagflag-brn": {
        "backgroundPosition": "-320px -32px"
    },
    "flagflag-bel": {
        "backgroundPosition": "-96px -32px"
    },
    "flagflag-syc": {
        "backgroundPosition": "-448px -352px"
    },
    "flagflag-mys": {
        "backgroundPosition": "-352px -288px"
    },
    "flagflag-lux": {
        "backgroundPosition": "-128px -256px"
    },
    "flagflag-stp": {
        "backgroundPosition": "-320px -384px"
    },
    "flagflag-esp": {
        "backgroundPosition": "0 -128px"
    },
    "flagflag-wsm": {
        "backgroundPosition": "-320px -448px"
    },
    "flagflag-sdn": {
        "backgroundPosition": "0 -384px"
    },
    "flagflag-vct": {
        "backgroundPosition": "-128px -448px"
    },
    "flagflag-reu": {
        "backgroundPosition": "-224px -352px"
    },
    "flagflag-uzb": {
        "backgroundPosition": "-64px -448px"
    },
    "flagflag-srb": {
        "backgroundPosition": "-288px -352px"
    },
    "flagflag-mtq": {
        "backgroundPosition": "-96px -288px"
    },
    "flagflag-dom": {
        "backgroundPosition": "-256px -96px"
    },
    "flagflag-hkg": {
        "backgroundPosition": "-320px -160px"
    },
    "flagflag-cog": {
        "backgroundPosition": "-192px -64px"
    },
    "flagflag-che": {
        "backgroundPosition": "-224px -64px"
    },
    "flagflag-cyp": {
        "backgroundPosition": "-64px -96px"
    },
    "flagflag-cpv": {
        "backgroundPosition": "-32px -96px"
    },
    "flagflag-pri": {
        "backgroundPosition": "-32px -352px"
    },
    "flagflag-dji": {
        "backgroundPosition": "-160px -96px"
    },
    "flagflag-blr": {
        "backgroundPosition": "-32px -64px"
    },
    "flagflag-khm": {
        "backgroundPosition": "-64px -224px"
    },
    "flagflag-lbn": {
        "backgroundPosition": "-384px -224px"
    },
    "flagflag-gin": {
        "backgroundPosition": "-64px -160px"
    },
    "flagflag-mus": {
        "backgroundPosition": "-224px -288px"
    },
    "flagflag-per": {
        "backgroundPosition": "-320px -320px"
    },
    "flagflag-cri": {
        "backgroundPosition": "-448px -64px"
    },
    "flagflag-gbr": {
        "backgroundPosition": "-256px -128px"
    },
    "flagflag-som": {
        "backgroundPosition": "-256px -384px"
    },
    "flagflag-bgr": {
        "backgroundPosition": "-160px -32px"
    },
    "flagflag-nic": {
        "backgroundPosition": "-64px -320px"
    },
    "flagflag-sur": {
        "backgroundPosition": "-288px -384px"
    },
    "flagflag-bmu": {
        "backgroundPosition": "-288px -32px"
    },
    "flagflag-jam": {
        "backgroundPosition": "-384px -192px"
    }
});